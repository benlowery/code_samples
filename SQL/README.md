# SQL

[SearchService.php](SearchService.php)

This was a classic example of a client curveball, they asked for a full text search after
previously deciding they didn't want it in that sprint, this was a short term fix before pulling in 
ElasticSearch.

PostgresSQL has a surprisingly capable full text/stemming system built in with some
huge improvements due in v10.
