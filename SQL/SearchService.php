<?php

namespace Meta\Core\Search;

use Meta\Utility\Base\BaseService;
use Carbon\Carbon;

class SearchService extends BaseService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function searchEverything($query_string)
    {

        $query_string = str_replace(' ', ' & ', $query_string);

        $content_pages = $this->searchContentPages($query_string);
        $news_pages = $this->searchNewsPage($query_string);
        $galleries = $this->searchGalleries($query_string);
        $events = $this->searchEvents($query_string);

        $results = array_merge($content_pages, $news_pages, $galleries, $events);

        usort($results, function ($a, $b) {
            $a_time = Carbon::createFromFormat("Y-m-d H:i:s", $a['d_created_at']);
            $b_time = Carbon::createFromFormat("Y-m-d H:i:s", $b['d_created_at']);
            return $a_time < $b_time;
        });

        return $results;
    }

    public function searchContentPages($query_string)
    {
        \DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);

        $sql = "SELECT
                  d_id,
                  d_title,
                  d_url,
                  d_created_at,
                  d_summary,
                  d_hide_from_searches
                FROM (SELECT
                        content_pages.id                                              AS d_id,
                        content_pages.title                                           AS d_title,
                        '/content/' || content_pages.url || '-' || content_pages.hash AS d_url,
                        content_pages.created_at                                      AS d_created_at,
                        substring(content_pages.content_page_plaintext from 1 for 200) as d_summary,
                        content_pages.hide_from_searches as d_hide_from_searches,
                        to_tsvector(content_pages.title) ||
                        to_tsvector(content_pages.content_page_plaintext)
                                                                                      AS document
                      FROM content_pages) d_search
                WHERE d_search.document @@ to_tsquery(:query_string) AND d_hide_from_searches = FALSE
                ORDER BY d_created_at DESC";

        $results = \DB::select(\DB::raw($sql), ['query_string' => $query_string]);

        \DB::connection()->setFetchMode(\PDO::FETCH_CLASS);

        foreach ($results as &$result) {
            $result['d_type'] = 'Page';
        }

        return $results;
    }

    public function searchNewsPage($query_string)
    {

        \DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);

        $sql = " SELECT
                  did,
                  d_title,
                  d_url,
                  d_created_at,
                  d_summary,
                  d_hide_from_searches
                FROM (SELECT
                        news_pages.id                                        AS did,
                        news_pages.title                                     AS d_title,
                        news_pages.hide_from_searches                        AS d_hide_from_searches,
                        '/news/' || news_pages.url || '-' || news_pages.hash AS d_url,
                        news_pages.created_at                                AS d_created_at,
                        substring(news_pages.news_page_plaintext from 1 for 200) as d_summary,
                        to_tsvector(news_pages.title) ||
                        to_tsvector(news_pages.news_page_plaintext)
                                                                             AS document
                      FROM news_pages) d_search
                WHERE d_search.document @@ to_tsquery(:query_string) AND d_hide_from_searches = FALSE
                ORDER BY d_created_at DESC";


        $results = \DB::select(\DB::raw($sql), ['query_string' => $query_string]);

        \DB::connection()->setFetchMode(\PDO::FETCH_CLASS);

        foreach ($results as &$result) {
            $result['d_type'] = 'Page';
        }

        return $results;
    }

    public function searchEvents($query_string)
    {
        \DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);

        $sql = "SELECT
                  did,
                  d_title,
                  d_url,
                  d_created_at,
                  d_summary,
                  d_hide_from_searches
                FROM (SELECT
                        events.id                                 AS did,
                        events.title                              AS d_title,
                        events.hide_from_searches                 AS d_hide_from_searches,
                        '/event/' || events.id                    AS d_url,
                        events.created_at                         AS d_created_at,
                        substring(events.description_plaintext from 1 for 200) as d_summary,
                        to_tsvector(events.title) ||
                        to_tsvector(events.description_plaintext) AS document
                      FROM events) d_search
                WHERE d_search.document @@ to_tsquery(:query_string) AND d_hide_from_searches = FALSE
                ORDER BY d_created_at DESC";
        $results = \DB::select(\DB::raw($sql), ['query_string' => $query_string]);

        \DB::connection()->setFetchMode(\PDO::FETCH_CLASS);

        foreach ($results as &$result) {
            $result['d_type'] = 'Page';
        }

        return $results;
    }

    public function searchGalleries($query_string)
    {

        \DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);

        $sql = "SELECT
                  did,
                  d_title,
                  d_url,
                  d_created_at,
                  d_summary,
                  d_hide_from_searches
                FROM (SELECT
                        galleries.id                                          AS did,
                        galleries.title                                       AS d_title,
                        galleries.hide_from_searches                          AS d_hide_from_searches,
                        '/gallery/' || galleries.url || '-' || galleries.hash AS d_url,
                        galleries.created_at                                  AS d_created_at,
                        substring(galleries.gallery_page_plaintext from 1 for 200) as d_summary,
                        to_tsvector(galleries.title) ||
                        to_tsvector(galleries.gallery_page_plaintext)         AS document
                      FROM galleries) d_search
                WHERE d_search.document @@ to_tsquery(:query_string) AND d_hide_from_searches = FALSE
                ORDER BY d_created_at DESC";

        $results = \DB::select(\DB::raw($sql), ['query_string' => $query_string]);

        \DB::connection()->setFetchMode(\PDO::FETCH_CLASS);

        foreach ($results as &$result) {
            $result['d_type'] = 'Page';
        }

        return $results;
    }
}
