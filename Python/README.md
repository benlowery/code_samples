# Python

[TestHarness.py](TestHarness.py)

This was an application specific layer over Selenium, I found that since the client side
was using consistent components/markup rather than using XPath/Selectors everywhere it was
nice to just abstract this.

You can see an example of this in action in 

[buildThermalProject.py](buildThermalProject.py)

I found the ability to quickly and accurately fill in forms with faker generated fake data during development
was hugely useful, particularly when the forms concerned where huge.

I later used TestHarness as part of integration testing for the whole project.
