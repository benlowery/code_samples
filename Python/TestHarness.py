#!/usr/bin/env python
import unittest, time
import envoy
import sys
import os
import time
import random
import re
import pprint

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from faker import Factory
# Add bespoke generators
from MetaProvider import MetaProvider

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestHarness(unittest.TestCase):
    def setUp(self):

        # sometimes we will want to not have tests login
        self.force_disable_login = False

        # when set to true the test will simulate a human speed input
        self.delay_active = False

        # lowest and highest delays to simulate a human level of data entry speed
        self.delay_low = 0.250
        self.delay_high = 0.250

        # kill extant chrome instances
        self.nuke()

        self.options = Options()

        self.options.add_argument('user-data-dir=/home/dev/.config/google-chrome/Testing')
        self.options.binary_location = '/opt/google/chrome-beta/google-chrome-beta'

        self.options.add_argument('--start-maximized')
        self.options.add_argument('--test-type')
        self.options.add_argument('--no-default-browser-check')
        self.options.add_argument('--disable-cache')
        self.options.add_argument('--disable-application-cache')
        self.options.add_argument('--disk-cache-size=0')
        self.options.add_argument('--media-cache-size=0')
        self.options.add_argument('--disable-gpu')

        self.browser = webdriver.Chrome(chrome_options=self.options)

        self.browser.maximize_window()
        self.browser.implicitly_wait(1)
        self.base_url('http://orion.co.uk/')

        self.fake = Factory.create('en_GB')

        self.fake.add_provider(MetaProvider)

        self.username = 'company1user1'
        self.password = 'password'

        hostname = os.uname()[1]
        # adapt default window position based on hostname
        if hostname == 'work':
            self.setSize(1210, 1080)
            self.browser.set_window_position(3120, 0)
        elif hostname == 'home':
            self.browser.maximize_window()
            self.setSize(1280, 1040)
            self.browser.set_window_position(3840, 20)
        elif hostname == 'vostro':
            self.setSize(1240, 860)
            self.browser.set_window_position(360, 0)

    def setSize(self, width, height):
        self.browser.set_window_size(width, height)

    def nuke(self):
        envoy.run('pkill chrome')

    def base_url(self, url):
        self.delay()

        self.base_url = url

    def setTextByName(self, name, text):
        self.delay()

        self.browser.find_element_by_name(name).clear()
        self.browser.find_element_by_name(name).send_keys(text)

    def setTextById(self, id, text):
        self.delay()

        self.browser.find_element_by_id(id).send_keys(text)

    def clickById(self, id):
        self.delay()

        self.browser.find_element_by_id(id).send_keys(Keys.ENTER)

    def clickByText(self, text):
        self.delay()

        self.browser.find_element_by_xpath("//button[contains(.,'" + text + "')]").send_keys(Keys.ENTER)

    def clickAnchorByText(self, text):
        self.delay()
        self.browser.find_element_by_xpath("//a[contains(.,'" + text + "')]").click()

    def uploadFile(self, id, filename):
        self.delay()

        self.setTextById(id + "-file-input", filename)
        time.sleep(0.5)

    def setFileById(self, id, filepath):
        self.delay()

        self.browser.find_element_by_id(id).send_keys(filepath)

    def setSelectTextById(self, id, value):
        self.delay()

        select = Select(self.browser.find_element_by_id(id))
        select.select_by_visible_text(value)

    def setSelectTextByName(self, name, value):
        self.delay()

        select = Select(self.browser.find_element_by_name(name))
        select.select_by_visible_text(value)

    def setSelectIndexById(self, id, index):
        self.delay()

        select = Select(self.browser.find_element_by_id(id))
        select.select_by_index(index)

    def setSelectIndexByName(self, name, index):
        self.delay()

        select = Select(self.browser.find_element_by_name(name))
        select.select_by_index(index)

    def setMultiSelectById(self, id, values):
        self.delay()

        select = Select(self.browser.find_element_by_id(id))
        print [o.text for o in select.options]

        for value in values:
            select.select_by_visible_text(value)

    def setMultiSelectByName(self, name, values):
        self.delay()

        select = Select(self.browser.find_element_by_name(name))
        print [o.text for o in select.options]

        for value in values:
            select.select_by_visible_text(value)

    def setCkeditorByName(self, name, text):
        self.delay()

        ckeditor = self.browser.find_element_by_css_selector('div.cke_editor_{0} div.cke_wysiwyg_div'.format(name))
        ckeditor.send_keys(text)

    def goto(self, url):
        # self.delay()

        full_url = self.base_url + url
        print "going to " + full_url
        self.browser.get(full_url)

    def assert_element_with_text_exists(self, text, message="Element should exist but doesn't"):
        if self.browser.find_elements_by_xpath("//*[contains(text(), '" + text + "')]"):
            exists = True
        else:
            exists = False

        self.assertTrue(exists, message)

    def assert_sys_msg_matches_regex(self, regex, message="Unable to find matching message"):
        messages = self.browser.find_elements_by_css_selector('.system-message p')

        r = re.compile(regex, re.IGNORECASE)

        matches = False
        for message in messages:
            s = r.search(message.text)
            if s:
                matches = True

        self.assertTrue(matches, message)

    def msg_value_pure(self, rstr, ignorecase=True):
        messages = self.browser.find_elements_by_css_selector('.system-message p')

        if len(messages) == 0:
            return None

        if ignorecase:
            r = re.compile(rstr, re.IGNORECASE)
        else:
            r = re.compile(rstr)

        for message in messages:
            s = r.search(message.text)
            if s:
                return s.groups()

        return None

    def msg_values(self, rstr, ignorecase=True):
        messages = self.browser.find_elements_by_css_selector('.system-message p')

        if len(messages) == 0:
            return None

        rstr = rstr.replace('<', '(?P<')
        rstr = rstr.replace('>', '>.*)')

        if ignorecase:
            r = re.compile(rstr, re.IGNORECASE)
        else:
            r = re.compile(rstr)

        for message in messages:
            s = r.search(message.text)
            if s:
                return s.groupdict()

        return None

    def delay(self, delay=None):

        if self.delay_active:
            uniform_delay = random.uniform(self.delay_low, self.delay_high)

            # set delays
            time.sleep(uniform_delay)

    def hold(self, delay=1):
        time.sleep(delay)

    def login(self, username_string='company1user', password_string='password'):

        if self.force_disable_login:
            return

        self.goto('system/login')
        username = self.browser.find_element_by_name('token')
        username.send_keys(username_string)
        password = self.browser.find_element_by_name('password')
        password.send_keys(password_string)
        username.send_keys(Keys.RETURN)

        wait = WebDriverWait(self.browser, 10)

        element = wait.until(EC.title_contains('Main Dashboard'))
