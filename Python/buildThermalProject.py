#!/usr/bin/env python

import unittest
import sys
import random

sys.path.append('/home/dev/projects/oriontests/lib')
from TestHarness import TestHarness


class ProjectAdd(TestHarness):
    def createProject(self):
        self.goto('thermal/project/add')

        self.setTextByName('project_reference', self.fake.project_reference())

        self.setSelectIndexByName('customer_id', 1)
        self.setSelectTextByName('use_customer_address', 'Yes')
        self.setSelectTextById('user_id', 'John Von Neuman')

        self.clickByText('Save New Customer')

        return True

    def createTelephoneSurvey(self, project_id):
        self.goto('thermal/{}/telephone-survey'.format(project_id))

        self.setSelectTextByName('installation_type', 'Existing Roof')
        self.setSelectTextByName('orientation', 'North')
        self.setSelectTextByName('roof_slope', 'Flat')
        self.setSelectTextByName('roof_condition', 'New Roof')
        self.setSelectTextByName('roof_obstructed', 'Yes')
        self.setTextByName('roof_description', self.fake.text(self.fake.randint(50, 100)))
        self.setSelectTextByName('roof_material', 'Natural Slate')
        self.setSelectTextByName('scaffold_erectable', 'Yes')
        self.setSelectTextByName('boiler_type', 'Gas Combination Boiler')
        self.setTextByName('boiler_make_model', 'Foo Solar 2000')
        self.setSelectTextByName('space_for_water_tank', 'Yes')
        self.setSelectTextByName('system_type', 'Unvented/Pressurised')
        self.setSelectTextByName('property_type', 'Flat')
        self.setSelectTextByName('loft_accessible', 'Yes')
        self.setSelectTextByName('conservation_status', 'Listed')
        self.setTextByName('telephone_notes', self.fake.text(self.fake.randint(50, 100)))

        self.clickByText('Save Telephone Survey')

        return True

    def createSiteSurvey(self, project_id):
        self.goto("thermal/{0}/site-survey".format(project_id))

        self.setSelectTextByName('site_usage', 'Domestic')
        self.setSelectTextByName('property_type', 'Semi')

        self.setTextByName('people_daily_use', self.fake.randint(1, 5))
        self.setTextByName('sinks', self.fake.randint(1, 3))
        self.setTextByName('baths', self.fake.randint(1, 3))
        self.setTextByName('showers', self.fake.randint(1, 3))

        self.setSelectTextByName('underfloor_heating', 'Yes')

        self.setSelectTextByName('monday', self.fake.highlow())
        self.setSelectTextByName('tuesday', self.fake.highlow())
        self.setSelectTextByName('wednesday', self.fake.highlow())
        self.setSelectTextByName('thursday', self.fake.highlow())
        self.setSelectTextByName('friday', self.fake.highlow())
        self.setSelectTextByName('saturday', self.fake.highlow())
        self.setSelectTextByName('sunday', self.fake.highlow())

        self.setSelectTextByName('january', self.fake.highlow())
        self.setSelectTextByName('february', self.fake.highlow())
        self.setSelectTextByName('march', self.fake.highlow())
        self.setSelectTextByName('april', self.fake.highlow())
        self.setSelectTextByName('may', self.fake.highlow())
        self.setSelectTextByName('june', self.fake.highlow())
        self.setSelectTextByName('july', self.fake.highlow())
        self.setSelectTextByName('august', self.fake.highlow())
        self.setSelectTextByName('september', self.fake.highlow())
        self.setSelectTextByName('october', self.fake.highlow())
        self.setSelectTextByName('november', self.fake.highlow())
        self.setSelectTextByName('december', self.fake.highlow())

        self.setTextByName('orientation', '180')

        self.setMultiSelectByName('shading_type', ['Building', 'Trees'])

        self.setSelectTextByName('shading_duration', 'Part of Day')

        self.setSelectTextByName('roof_type', self.fake.roof_types())

        self.setSelectTextByName('roof_or_panel', 'Roof')
        self.setTextByName('angle', '45')

        self.setSelectTextByName('flat_or_pitched', 'Flat')

        self.setTextByName('roof_width', '20')
        self.setTextByName('roof_height', '20')

        self.setSelectTextByName('new_cylinder_required', self.fake.yes_no())

        self.setSelectTextByName('boiler_type', self.fake.survey_boiler_type())

        self.setTextByName('cylinder_age', self.fake.randint(5, 20))
        self.setTextByName('cylinder_location', random.choice(['Loft', 'Airing Cupboard']))
        self.setTextByName('collector_distance', self.fake.randint(5, 20))

        self.setSelectTextByName('primary_heat', self.fake.survey_fuel_type())

        self.setTextByName('primary_heat_output', self.fake.randint(1, 5))

        self.setMultiSelectByName('heating_system_description', ['Boiler', 'Radiators'])

        self.setSelectTextByName('domestic_hw_generation', 'System Boiler')

        self.setTextByName('existing_demand', self.fake.randint(1000, 2500))

        self.setSelectTextByName('planning_permission', self.fake.yes_no())

        text = self.fake.text(self.fake.randint(50, 100))
        self.setCkeditorByName('other_notes', text)

        self.setSelectIndexByName('performed_by', '1')

        self.clickByText('Save Site Survey')

        return True

    def createSapCalculation(self, project_id):
        self.goto("thermal/{}/sap-calculation".format(project_id))

        self.setTextByName('collector_manufacturer', 'Foobar Widgets')
        self.setTextByName('collector_model', 'Sunsucker 2000')
        self.setTextByName('controller_manufacturer', 'BarFiz Electronics')
        self.setTextByName('controller_model', 'Fizzy2000')

        self.setSelectTextByName('evacuated_tube', 'No')
        self.setTextByName('total_aperture', '6')
        self.setTextByName('n0', '0.64')
        self.setTextByName('a1', '4.41')
        self.setTextByName('a2', '0.008')

        self.setSelectTextByName('panel_shading', 'None/Little (<20%)')

        self.setTextByName('region', '11')
        self.setTextByName('orientation', '180')
        self.setTextByName('tilt', '45')

        self.setSelectTextByName('shower_type', 'No Shower, Bath Only')
        self.setSelectTextByName('water_efficiency', 'Other Dwelling')
        self.setSelectTextByName('boiler_type', 'Oil, Standard, Post 1998')
        self.setSelectTextByName('fuel_type', 'Oil')

        self.setTextByName('cylinder_manufacturer', 'Spherical Cylinder')
        self.setTextByName('cylinder_model', 'Mk3 22122')
        self.setTextByName('total_volume', '200')
        self.setTextByName('dedicated_solar_volume', '50')

        self.setSelectTextByName('occupied', 'No')
        self.setTextByName('building_area', '200')

        self.hold()

        self.clickByText('Save SAP Calculation')

        return True

    def createSystemQuote(self, project_id):
        self.goto('thermal/{}/system-quote/add'.format(project_id))

        self.setTextByName('quote_reference', self.fake.number_string())

        self.clickByText('Add Quote Line/Item')
        self.setTextByName('ko_unique_1', 'Solar Panels')
        self.setTextByName('ko_unique_2', '10')
        self.setTextByName('ko_unique_3', '49.99')
        self.setSelectTextByName('ko_unique_4', '5%')

        self.clickByText('Add Quote Line/Item')
        self.setTextByName('ko_unique_5', 'Piping')
        self.setTextByName('ko_unique_6', '2')
        self.setTextByName('ko_unique_7', '17.99')
        self.setSelectTextByName('ko_unique_8', '20%')

        self.clickByText('Add Quote Line/Item')
        self.setTextByName('ko_unique_9', 'Cylinder')
        self.setTextByName('ko_unique_10', '1')
        self.setTextByName('ko_unique_11', '199.99')
        self.setSelectTextByName('ko_unique_12', 'Exempt')

        self.hold()
        self.clickByText('Save System Quote')

        return True

    def createPurchaseOrder(self, project_id):
        self.goto('project/thermal/{}/system-purchase-order/add'.format(project_id))

        self.setTextByName('purchase_order_ref', self.fake.number_string())
        self.setSelectIndexById('user_id', 1)

        self.setSelectIndexById('supplier', 1)
        self.setSelectIndexById('address', 1)

        self.setTextByName('line1', self.fake.street_address())
        self.setTextByName('line2', self.fake.secondary_address())
        self.setTextByName('city', self.fake.city())
        self.setTextByName('region', self.fake.region())
        self.setTextByName('postcode', self.fake.postcode())

        self.clickByText('Add Item')

        self.setSelectIndexByName('ko_unique_1', 1)
        self.setTextByName('ko_unique_2', 5)
        self.setSelectIndexByName('ko_unique_3', 1)
        self.setTextByName('ko_unique_4', '49.99')

        self.clickByText('Add Item')

        self.setSelectIndexByName('ko_unique_5', 2)
        self.setTextByName('ko_unique_6', 10)
        self.setSelectIndexByName('ko_unique_7', 2)
        self.setTextByName('ko_unique_8', '99.99')

        self.clickByText('Save Purchase Order')

        return True

    def receivePurchaseOrder(self, project_id):
        self.setTextByName('ko_unique_3', '5')
        self.setTextByName('ko_unique_6', '5')
        text = self.fake.text(self.fake.randint(50, 100))
        self.setCkeditorByName('notes', text)

        self.clickByText('Accept Purchase Order')

    def riskAssessment(self, project_id):
        self.goto("thermal/{0}/risk-assessment".format(project_id))

        self.clickByText('Add Risk Item')

        self.setSelectIndexById('performed_by', 1)

        self.setTextByName('hazard_identified', 'Falling')
        self.setTextByName('people_exposed', 'Employees')
        self.setTextByName('action_taken', 'Railings and Harnesses Provided')
        self.setTextByName('ppe', 'Safety Harnesses')
        self.setSelectTextByName('hazard_severity', 'High')
        self.setSelectTextByName('hazard_chance', 'Low')
        self.setSelectTextByName('hazard_risk', 'Low')
        self.setTextByName('training', 'Working at Height Regulations')
        self.setTextByName('assessment', 'N/A')

        self.clickByText('Save Risk Assessment')

        return True

    def test_thermal_project_add(self):
        self.login()

        self.createProject()
        self.hold()
        self.assert_sys_msg_matches_regex('^Project .* was created .*$')
        # stash project_id as it's used over and over
        message = self.msg_values('Project :- <project_ref> was created with ID: <project_id>')
        self.project_id = message['project_id']

        self.createTelephoneSurvey(self.project_id)
        self.hold()
        self.assert_sys_msg_matches_regex('^telephone survey was saved$')

        self.createSiteSurvey(self.project_id)
        self.hold()
        self.assert_sys_msg_matches_regex('^site survey was updated$')

        self.createSapCalculation(self.project_id)
        self.hold()
        self.assert_sys_msg_matches_regex('^Sap Calculation Saved$')

        self.createSystemQuote(self.project_id)
        self.hold()
        self.assert_sys_msg_matches_regex('^New quote was created$')

        self.createPurchaseOrder(self.project_id)
        self.hold()
        self.assert_sys_msg_matches_regex('^Purchase Order .* was created$')

        self.clickByText('PO Actions')
        self.clickAnchorByText(' Mark as Sent')
        self.clickByText('OK')

        self.clickByText('Goods Receiving')
        self.clickAnchorByText('Receive Purchase Order')
        self.hold()

        self.receivePurchaseOrder(self.project_id)
        self.hold()

        self.riskAssessment(self.project_id)
        self.hold()

        return True


if __name__ == '__main__':
    unittest.main(verbosity=2, argv=sys.argv[:1])
