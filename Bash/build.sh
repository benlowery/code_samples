#!/bin/bash
# Activate Recursive Globbing
shopt -s globstar

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

#Useful to avoid relative path hell in includes, require() is now relative to js
ADDITIONAL_NODE_PATH="$PWD/js"
NODE_PATH="$PWD/node_modules":"$ADDITIONAL_NODE_PATH"


BASE_PATH="/home/dev/projects/lufi/mercury/site"
ADMIN_PATH="$BASE_PATH/client/admin"

NODE_MODULES="$ADMIN_PATH/node_modules"

JS_APP_INPUT="/client/admin/js/app"
JS_APP_OUTPUT="/public/app/admin/js/app"

BUNDLE_JS_FILE="$BASE_PATH/public/app/admin/js/lib/bundle.js"

LIBRARY_JS_FILE="$BASE_PATH/public/app/admin/js/lib/library.js"
CKEDITOR_JS_FILE="$BASE_PATH/public/app/admin/js/lib/ckeditor-library.js"
DATATABLES_JS_FILE="$BASE_PATH/public/app/admin/js/lib/datatables-library.js"

LIBRARY_MODULES="jquery lodash knockout knockout.validation"
CKEDITOR_MODULES="ckeditor"
DATATABLES_MODULES="datatables.net datatables.net-bs"

PUBLIC_ROOT="$BASE_PATH/public/app/admin"
RESOURCES_ROOT="$PUBLIC_ROOT/resources"

LESS_IN="$ADMIN_PATH/css/main.less"
LESS_OUT="$BASE_PATH/public/app/admin/css/application.css"

BROWSERIFY_COMMAND="$PWD/node_modules/.bin/browserify"
WATCHIFY_COMMAND="$PWD/node_modules/.bin/watchify"
LESSC_COMMAND="$PWD/node_modules/.bin/lessc"

DEBUG_MODE=true

if [[ $* == *--production* ]]; then
    DEBUG_MODE=false
    echo "***** Production Mode ******"
fi

if [ "$DEBUG_MODE" = true ]; then
    echo "***** DEBUG MODE ON!!! *****"
fi

if [[ $* == *--watchify* ]] || [[ $* == *--watch* ]]; then
    echo "Watch Mode";
    BROWSERIFY_COMMAND="$WATCHIFY_COMMAND"
fi

function bytesToHuman() {
    b=${1:-0}; d=''; s=0; S=(Bytes {K,M,G,T,E,P,Y,Z}iB)
    while ((b > 1024)); do
        d="$(printf ".%02d" $((b % 1024 * 100 / 1024)))"
        b=$((b / 1024))
        let s++
    done
    echo "$b$d ${S[$s]}"
}

function create_dir {
    mkdir -p "${1%/*}"
}

function build_js {
    echo "Building Application"
    # build string like -x foo -x bar

    excluded_modules=""
    modules_to_exclude="${LIBRARY_MODULES[@]} ${CKEDITOR_MODULES[@]} ${DATATABLES_MODULES[@]}"
    for module in $modules_to_exclude
    do
        excluded_modules="$excluded_modules -x $module"
    done

    for ts_input_filename in "$BASE_PATH$JS_APP_INPUT"/**/*.ts;
    do
        # replace the app input path with the app output path (final location)
        ts_output_full_path=${ts_input_filename/"$JS_APP_INPUT"/"$JS_APP_OUTPUT"}

        # glob the output directory and make it since browserify won't directories for us
        create_dir "$ts_output_full_path"

        # build the input string to browserify /foo/bar.js /fizz/buzz.js
        ts_in_string="$ts_in_string $ts_input_filename"

        # build the output map -o /foo/bar.js -o /fizz/buzz.js
        out_string="$out_string -o $ts_output_full_path"
    done

    for js_input_filename in "$BASE_PATH$JS_APP_INPUT"/**/*.js;
    do
        # replace the app input path with the app output path (final location)
        output_full_path=${js_input_filename/"$JS_APP_INPUT"/"$JS_APP_OUTPUT"}

        # glob the output directory and make it since browserify won't directories for us
        create_dir "$output_full_path"

        # build the input string to browserify /foo/bar.js /fizz/buzz.js
        in_string="$in_string $js_input_filename"

        # build the output map -o /foo/bar.js -o /fizz/buzz.js
        out_string="$out_string -o $output_full_path"
    done

    if [ "$DEBUG_MODE" = true ]; then
        browserify_debug_flag="--debug"
    fi

    if [ "$DEBUG_MODE" = false ]; then
        uglify_string="-g uglifyify"
    fi

    create_dir "$BUNDLE_JS_FILE"
    # As we are using factor bundle and ts is turned to js we need to replace .ts with .js *only* in the out string
    out_string=${out_string//".ts"/".js"}
    command="$BROWSERIFY_COMMAND $uglify_string $browserify_debug_flag $ts_in_string $in_string $excluded_modules -p [factor-bundle $out_string ] -p [ tsify --baseUrl $DIR/js --project $DIR/tsconfig.json --allowJs ] -o $BUNDLE_JS_FILE -t [ babelify --sourceMapsAbsolute --presets [ es2015-without-strict ] ] -t brfs -v"

    echo "$command"
    eval "$command"
}

function build_library {
    # last element is output location
    local output_js_location="${@: -1}"
    # everything but the last element
    local local_library_modules="${@:1:$(($#-1))}"

    echo "Building: $output_js_location"

    modules_string=""
    module=""
    echo "${local_library_modules[@]}"

    for module in ${local_library_modules[@]}
    do
        modules_string="$modules_string -r $module"
    done



    create_dir "$output_js_location"

    if [ "$DEBUG_MODE" = false ];
    then
        echo "uglifying!"
        local uglify_string="-g uglifyify"
    else
        local uglify_string=""
    fi

    local command="$BROWSERIFY_COMMAND $uglify_string $modules_string -o $output_js_location"
    echo "$command"
    eval $command
}

function build_css {
    echo "Building CSS"

    if [ "$DEBUG_MODE" = true ]; then
        less_debug_flag="--source-map"
    fi

    create_dir "$LESS_OUT"
    command="$LESSC_COMMAND $less_debug_flag  $LESS_IN $LESS_OUT"
    echo "$command"
    eval "$command"
    size=$(bytesToHuman $(stat -c%s "$LESS_OUT"))

    echo "Size: $size"

}

# simply deploying assets from node_modules reliably (yay Yarn for locking properly)
function build_static {

    echo "Handling Static Assets"

    # Move material design iconic font
    create_dir "$RESOURCES_ROOT/material-design-iconic-font"
    cp -r "$NODE_MODULES/material-design-iconic-font/dist" "$RESOURCES_ROOT/material-design-iconic-font/"

    # move font awesome fonts to correct place in public
    cp -r "$NODE_MODULES/font-awesome/fonts" "$PUBLIC_ROOT/"
    cp -r "$NODE_MODULES/bootstrap/fonts" "$PUBLIC_ROOT/"

}

function nuke {

    # test we have it and kill it if we do
    if [ -d "$PUBLIC_ROOT" ]; then
        rm -r "$PUBLIC_ROOT"
    fi
}

function library {
    build_library "${LIBRARY_MODULES[@]}" "$LIBRARY_JS_FILE"
    build_library "${CKEDITOR_MODULES}" "$CKEDITOR_JS_FILE"
    cp -r "$ADMIN_PATH/js/staticjs" "$PUBLIC_ROOT/js/"

    build_library "${DATATABLES_MODULES}" "$DATATABLES_JS_FILE"
}

case $1 in
"js")
    start=$(($(date +%s%N)/1000000)) # start time in milliseconds
    build_js
    end=$(($(date +%s%N)/1000000)) # end time in milliseconds
    time_taken=$(bc -l <<< "scale=2; ( ${end} - ${start}) / 1000")
    notify-send -t 4 -i "/usr/share/icons/elementary-xfce/actions/128/edit-redo.png" "Done at $(date +"%T") took: ${time_taken}s"
    xrefresh -solid white

    ;;
"library")

    library
    ;;
"css")
    build_css
    build_static
    ;;
"static")
    build_static
    ;;
"nuke")
    nuke
    ;;
"all")
    library
    build_js
    build_css
    build_static
    ;;
*)
    echo "No command, ./build.sh css, ./build.sh js --watchify --debug, ./buildjs library --watchify --debug"
esac
