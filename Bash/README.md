# BASH Scripts

Because of the existing application architecture I've inherited had some fairly unique requirements in how
the front end was built, this project moved from npm to yarn and js to typescript.

In concept it creates library.js, bundle.js and some component specific bundles (ckeditor etc)

Browserify does the underlying work via the CLI interface.

This was before WebPack was as complete as the newer versions, I'd probably use WebPack for this
now, though this approach does have the advantage that bash is easy to debug, not something I can 
say about WebPack at times.

This script allows you to do things like

    ./build.sh js // build the entire client side js/typescript fabric and deploy into public
    ./build.sh js --watch // incrementally build js/typescript
    ./build.sh css // build custom bootstrap theme
    ./build.sh library // build the core 'library' module bundle
