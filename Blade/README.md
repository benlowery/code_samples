# Blade/Blade Components

Blade components to encapsulate recurring form functionality.

This is an extension of a method I've used for some time http://benlowery.co.uk/blog/blade_components.html
taking of advantage of the new @component() functionality in the latest version of Laravel.

It makes refactoring a lot easier when your form inputs/components work like this
 
     @component('Components::inputs.text',[
        'name' => 'email_address',
        'label' => 'Email Address',
        'placeholder' => 'Required Field, Used for login'
    ])
    @endcomponent
