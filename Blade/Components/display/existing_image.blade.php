<div class="form-group">
    @if(isset($label)) <label class="control-label">{{ $label }}</label> @endif
    <div class="form-group existing-image">
        <div class="existing-image__inner">
            <a data-bind="attr:{ href: {{ $name or $property }}()}" target="_blank">
                <img class="img img-responsive center-block" data-bind="attr:{src: {{ $name or $property }}() + '?options={{ $options }}' }">
                View Original
            </a>
        </div>
    </div>
</div>
