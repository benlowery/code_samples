<div class="btn-group">
    <button type="button" class="btn btn-sm btn-m-{{ $color }} dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ $title }} <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        {{ $slot }}
    </ul>
</div>
