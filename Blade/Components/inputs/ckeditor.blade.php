<?php $required = $required ?? false ?>

<div class="form-group" @if($required == true) data-bind="validationElement: {{ $property or $name }}" @endif>

    @if(isset($label))<label class="control-label" for="{!! $name !!}">{!! $label or '' !!}</label>@endif
    @if($required === true)
        <div data-bind="validationMessage: {{ $property or $name }}" class="help-block"></div>
    @endif
    {{--options is called config because options is already reserved by the select plugin--}}
    <textarea
            data-bind="ckEditor: {{ $property or $name }}, config: {!! $options or '{height: 200}' !!} "
            class="form-control"
            name="{{ $name }}"
            id="{{ $name }}"
            placeholder="{{ $placeholder or '' }}"
    >

    </textarea>
    @if($required === true)
        <div data-bind="validationMessage: {{ $property or $name }}" class="help-block"></div>
    @endif
</div>
