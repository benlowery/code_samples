{{-- $name (string), $label (string), optional - $property, $placeholder  --}}

<div data-bind="validationElement: {{ $property or $name }}" class="form-group">
    <label class="control-label" for="title">{{ $label or '' }}</label>

    <textarea data-bind="value: {{ $property or $name }}"
              id="{{ $name }}"
              name="{{ $name }}"
              type="text"
              class="form-control textarea_full-height"
              placeholder="{{ $placeholder or '' }}"></textarea>

    @if(isset($help))
        <p class="help-block">{{ $help }}</p>
    @endif
</div>

