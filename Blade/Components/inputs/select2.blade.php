<div class="form-group" data-bind="validationElement: {{ $value }}">
    <label for="{{ $value }}" class="control-label">{{ $label }}</label>
    <select data-bind="options: {{ $options }},
                                        value: {{ $value }},
                                        @if(isset($optionsCaption)) optionsCaption: '{{ $optionsCaption or ''}}', @endif
            optionsValue: '{{ $optionsValue}}',
                                        optionsText: '{{ $optionsText}}',
                                        select2:{}"
            name="{{ $value }}"
            class="form-control"
            id="{{ $value }}"></select>
</div>

