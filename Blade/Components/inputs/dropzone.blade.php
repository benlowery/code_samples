<div data-bind="validationElement: {{ $name }}" class="form-group">
    @if(isset($label))
        <label class="control-label">{{ $label }}</label>
    @endif
    <span data-bind="validationMessage: {{ $name }}" class="help-block"></span>
    <div class="dropzone {{ $class or '' }}" data-bind="dropzone: { value: {{ $name }} , url: '/system/upload/dropzone', maxFiles: {{ $maxFiles or '1' }}}"></div>
</div>
