{{-- $name (string), $label (string), optional - $property, $placeholder  --}}

<div data-bind="validationElement: {{ $property or $name }}" class="form-group">
    <label class="control-label" for="title">{{ $label or '' }}</label>

    <input data-bind="value: {{ $property or $name }}"
           id="{{ $name }}"
           name="{{ $name }}"
           type="text"
           class="form-control"
           placeholder="{{ $placeholder or '' }}">

    @if(isset($help))
        <p class="help-block">{{ $help }}</p>
    @endif
</div>

