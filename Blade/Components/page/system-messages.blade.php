<?php if (\Session::has('messages')): ?>
<div class="row system-message__container">
    <div class="col-xs-24">
        @foreach(\Session::get('messages') as $message)


            <div class="system-message alert alert-<?php echo $message['type']; ?> alert-dismissable">
                <button type="button"
                        class="btn btn-primary close"
                        data-dismiss="alert">
                    x
                </button>
                <h4 class="no-bottom"><?= $message['message'] ?></h4>
            </div>


        @endforeach
    </div>
</div>
<?php endif ?>
