@if( config('app.debug') === true)
    <div class="well">
        <div class="row">
            <div class="col-lg-9">
                <pre data-bind="text: ko.toJSON($data, null, 2)"></pre>
            </div>
            <div class="col-lg-9">
                <pre data-bind="text: $root.debugify()"></pre>
            </div>
        </div>


    </div>
@endif
