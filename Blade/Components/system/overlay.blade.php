@if(config('app.debug'))

    <style>
        #debug-overlay {
            padding: 8px;
            background: lightblue;
            position: fixed;
            top: 0;
            right: 0;
            z-index: 1000;
            width: 40px;
        }

        #debug-overlay.is-shown {
            width: 50%;
        }

        #debug-overlay .contents {
            margin-top: 8px;
            display: none;
        }

        #debug-overlay.is-shown .contents {
            display: block;
        }

    </style>

    <div id="debug-overlay" class="col-lg-9 .visible">
        <button id="show-hide" class="btn btn-primary is-shown"></button>

        <div class="contents">
            <pre id="pre-debugify" data-bind="text: $root.debugify()"></pre>
            <div>
                <button id="log-state-to-console">Log State to Console</button>
            </div>
        </div>
    </div>

    <script>

        // set a cookie to remember the debug overlay state
        function cookie_set(cookie_name, value, exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value = encodeURI(value) + ((exdays == null)
                    ? "" : "; expires=" + exdate.toUTCString())
                + "; path=/";
            document.cookie = cookie_name + "=" + c_value;
        }

        // return value for cookie name
        function getCookie(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }

        document.getElementById("log-state-to-console").addEventListener("click", function () {

            var json = document.getElementById("pre-debugify").innerText;
            console.log(JSON.parse(json));

        });

        document.getElementById("show-hide").addEventListener("click", function (e) {
                var element = e.target;
                var parentElement = e.target.parentElement;
                parentElement.classList.toggle('is-shown');

                if (parentElement.classList.contains('is-shown')) {
                    element.innerText = ">";
                    cookie_set("debug-overlay", true, 30);
                }
                else {
                    element.innerText = "<";
                    cookie_set("debug-overlay", false, 30);
                }
            }
        );
        var element = document.getElementById("show-hide");

        if (getCookie('debug-overlay') === "true") {
            element.innerText = ">";
            element.parentElement.classList.add('is-shown')
        }
        else {
            element.innerText = "<";
            element.classList.remove('is-shown');
        }
        console.log(getCookie('debug-overlay'));

    </script>
@endif
