/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

var defaultProfile = [
    {
        name: 'clipboard',
        items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
    }];

var advancedProfile = [
    {
        name: 'clipboard',
        items: ['Cut', 'Copy', 'PasteText', 'Paste', 'PasteFromWord', '-', 'Undo', 'Redo']
    },
    {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll']},
    {
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline', '-', 'CopyFormatting', 'RemoveFormat']
    },
    {
        name: 'paragraph',
        items: ['NumberedList', 'BulletedList', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
    },
    {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
    {
        name: 'insert',
        items: ['Image', 'PageBreak', 'EmbedSemantic', 'Div']
    },
    {name: 'foo', items: ['embed']},
    {name: 'styles', items: ['Format', 'Styles', 'Font', 'FontSize']},
    {name: 'tools', items: ['Maximize']},
    {
        name: 'document',
        items: ['Source']
    }
];


module.exports = function (options) {
    // default to an empty toolbar if the option is not specified
    options.toolbar = [];

    options.stylesSet = [
        {name: 'Make Circular', type: 'widget', widget: 'image', attributes: {'class': 'circular-img'}},
        {name: 'Polaroid', type: 'widget', widget: 'image', attributes: {'class': 'polaroid-img'}},
        {name: 'Lightbox', type: 'widget', widget: 'image', attributes: {'class': 'show-in-lightbox'}}
    ];

    options.height = _.isUndefined(options.height) ? 1000 : options.height;
    options.minHeight = _.isUndefined(options.minHeight) ? 50 : options.minHeight;
    options.toolbar = _.isUndefined(options.toolbar) ? "null" : options.toolbar;
    options.htmlbuttons = _.isUndefined(options.htmlbuttons) ? {} : options.htmlbuttons;
    options.profile = _.isUndefined(options.profile) ? 'default' : options.profile;

    var haveProfile = false;

    if (options.profile === "default") {
        haveProfile = true;
        options.toolbar = defaultProfile;
    }

    if (options.profile === "advanced") {
        haveProfile = true;
        options.toolbar = advancedProfile;
    }

    if (haveProfile !== true) {
        alert("No selected profile for ckeditor");
    }

    options.allowedContent = true;
    options.image2_alignClasses = ['cke-img-left', 'cke-img-middle', 'cke-img-right'];
    options.image2_captionedClass = 'image-captioned';
    options.uploadUrl = '/system/upload/ajax';
    options.filebrowserUploadUrl = '/system/upload/file' + '?_token=' + global.CSRF_TOKEN;
    options.extraPlugins = 'divarea,widget,uploadimage,embedbase,embedsemantic'; // these are important
    options.embed_provider = "http://iframely.metasoftware.co.uk:8061/oembed?url={url}&callback={callback}";

    //options.enterMode = CKEDITOR.ENTER_BR;
    // The passed in options object is used here to build the return object
    // the alternative would be to extend the object but this way is cleaner
    // as you can see exactly what it passed to and for
    return options;
}
;


