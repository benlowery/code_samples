var $ = global.jQuery;
var ko = require('knockout');
var select2 = require('select2');


var Select2Component = function () {
    var self = this;

    self.init = function (element, valueAccess, allBindings, viewModel, bindingContext) {

        var options = allBindings().select2 || {};

        options.theme = "bootstrap";
        options.width = '';

        $(element).select2(options);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).select2('destroy');
        })


    };
    self.update = function (element, valueAccessor, allBindings) {
        $(element).val(ko.utils.unwrapObservable(allBindings().value()));
        $(element).trigger('change');
    }
};

ko.bindingHandlers.select2 = new Select2Component();

