var ko = require('knockout');
var $ = require('jquery');
var _ = require('lodash');

// ENSURE YOU PASS WINDOW AND $ TO THE DAMN THING OTHERWISE IT USES THE WRONG SCOPE FOR $
var dt = require('datatables.net')(window, global.jQuery);
var dtb = require('datatables.net-bs')(window, global.jQuery);

var metaTable = function () {
    var self = this;

    self.init = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var bindingOptions = valueAccessor();

        var options = {};

        // merge all bindingOptions (arguments passed to metaTable) onto options
        $.extend(options, bindingOptions);

        if (!options.autoWidth) {
            options.autoWidth = false;
        }

        if (!options.sDom) {
            options.sDom = "sDom: '<\'row\'<\'meta-datatable\'<\'col-xs-offset-18 col-xs-6 search\'f><\'col-xs-24 main-table\'t><\'col-xs-6 info\'i><\'col-xs-12 pagination-ctn\'p><\'col-xs-6 view-count\'l>>'";
        }

        // if it exists and we have columns
        if (bindingOptions.columns && bindingOptions.columns.length) {
            options.aoColumns = [];
            ko.utils.arrayForEach(bindingOptions.columns, function (column) {
                options.aoColumns.push(column);
            });
        }

        if (!_.isUndefined(bindingOptions.rowTemplate)) {
            options.fnRowCallback = function (row, data, displayIndex, displayIndexFull) {
                // Render the row template for this row.
                // note the call to createChild Context to handle setting the parents
                ko.renderTemplate(bindingOptions.rowTemplate, bindingContext.createChildContext(data), null, row, "replaceChildren");
                return row;
            }
        }

        if (bindingOptions.dataSource) {
            var unwrapped = ko.utils.unwrapObservable(bindingOptions.dataSource);
            if (unwrapped instanceof Array) {
                options.aaData = unwrapped;
            }
        }

        bindingOptions.dataSource.subscribe(function (newItems) {

            var dataTable = $(element).dataTable();
            var api = dataTable.api();

            var tableNodes = api.rows().nodes();

            if (tableNodes.length) {
                ko.utils.arrayForEach(tableNodes, function (node) {
                    ko.cleanNode(node);
                });
            }
            dataTable._fnAjaxUpdateDraw({
                aaData: newItems
            });

        });


        $(element).dataTable(options);

        return {controlsDescendantBindings: true};

    };

    self.update = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
};

ko.bindingHandlers.metaTable = new metaTable();
