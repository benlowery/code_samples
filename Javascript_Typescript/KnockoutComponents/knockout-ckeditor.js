var ko = require('knockout');
var $ = global.jQuery;
var _ = require('lodash');
var cked = require('ckeditor');
var jqueryCreator = require('jqueryCreator');
var pubsub = require('pubsub-js');

var ckconfig = require('./knockout-ckeditor-config.js');

var ckEditorComponent = function () {

    var self = this;

    self.init = function (element, valueAccessor, allBindingsAccessor, viewModel) {



        var txtBoxID = $(element).attr("id");

        var options = allBindingsAccessor().config || {};

        var finalConfigOptions = ckconfig(options);

        var editor = $(element).ckeditor(finalConfigOptions);

        // set initial value if it exists
        var observable = valueAccessor();
        var html_value = $(element).val(); // get the value from the text area if it has one

        var thisInstance = CKEDITOR.instances[txtBoxID];

        thisInstance.on('instanceReady', function (ev) {

            ev.editor.setKeystroke(CKEDITOR.CTRL + 86, 'italic');

            thisInstance.widgets.registered.uploadimage.onUploaded = function (upload) {


                var width = upload.responseData.width;
                var height = upload.responseData.height;
                var originalName = upload.responseData.filename;

                if (width < 500) {
                    pubsub.publish('com.page.growl', {message: `${originalName} is <500px wide, it may not look very good when used on a blog post`, type: "warning"});
                }

                var aspectRatio = height / width;

                var newWidth = 851;
                var newHeight = Math.round(newWidth * aspectRatio);

                this.replaceWith('<p class="cke-img-middle"><img src="' + upload.url + '" ' +
                    'width="' + newWidth + '" ' +
                    'height="' + newHeight + '"></p>');
            }

        });


        thisInstance.on('key', function (evt) {
            var keyCode = evt.data.keyCode;
            if (keyCode === CKEDITOR.CTRL + 86) {// Ctrl+V

                thisInstance.execCommand('pastetext');
                evt.cancel();
            }
        }, null, null, 1);

        thisInstance.on('fileUploadRequest', function (evt) {

            var xhr = evt.data.fileLoader.xhr;
            console.log(xhr);

            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('X-CSRF-Token', global.CSRF_TOKEN);
            xhr.withCredentials = true;
        });

        thisInstance.on('fileUploadResponse', function (evt) {


            if (evt.data.fileLoader.xhr.status === 200) {

                thisInstance.fire('change');
            }

        });

        thisInstance.on('change', function () {
            observable($(element).val());
        });

        // wire up the blur event to ensure our observable is properly updated
        thisInstance.focusManager.blur = function () {
            observable($(element).val());
        };

        // handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            if (CKEDITOR.instances[txtBoxID]) {
                CKEDITOR.remove(CKEDITOR.instances[txtBoxID]);
            }
        });

        CKEDITOR.on('dialogDefinition', function (ev) {

            // Take the dialog name and its definition from the event data.
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;

        });

    };

    self.update = function (element, valueAccessor, allBindingsAccessor, viewModel) {

        var val = ko.utils.unwrapObservable(valueAccessor());

        // if the value doesn't match the current data then update the new data
        if ($(element).ckeditorGet().getData() != val) {
            $(element).ckeditorGet().setData(val);
        }

    }
};

ko.bindingHandlers.ckEditor = new ckEditorComponent();
