require('main.js');
var Dropzone = require('dropzone');
var $ = global.jQuery;
var _ = require('lodash');
var pubsub = require('pubsub-js');


Dropzone.autoDiscover = false;

ko.bindingHandlers.dropzone = {
    init: function (el, opts) {
        opts = opts() || {};

        var removeImage = function (imageUrl) {
            return $.ajax(
                {
                    url: "/system/delete/file",
                    type: 'POST',
                    data: {
                        _method: 'delete',
                        filename: imageUrl
                    }
                }
            );
        };

        var dropzoneInit = function () {


            // initial value is an array and has more than 0 entires
            // loop the array and add them to dropzone.
            if (opts.value().length > 0) {

                _.forEach(opts.value(), (image) => {
                    // do something nasty to retrieve the filename
                    var filename = image.split('/').pop().split('#')[0].split('?')[0];

                    // slap a parameter to set the max height
                    var existingImageUrl = image + "?options=x120";

                    // accepted equal true so dropzone sees the mockfile in total count
                    var mockFile = {name: filename, serverUrl: image, size: 0, type: 'image/jpeg', accepted: true};

                    this.files.push(mockFile);
                    this.options.addedfile.call(this, mockFile);
                    this.options.thumbnail.call(this, mockFile, existingImageUrl);

                    // append the display classes
                    mockFile.previewElement.classList.add('dz-complete');
                    mockFile.previewElement.classList.add('dropzone-small');
                });


            }

            this.on('error', function (file, err) {
                console.log(err);

                // not that helpful...
                if (err === 'You can not upload any more files.') {
                    err = `You have reached the file limit of ${opts.maxFiles}, You need to remove files first`;
                }

                var error_string = file.name + " " + err;
                pubsub.publish('com.page.growl', {message: error_string, type: "warning"});
                pubsub.publish('com.page.message', {message: error_string, type: "warning"});
            });

            this.on('success', function (file, resp) {
                opts.value.push(resp.url);
            });


            this.on("maxfilesexceeded", function (file) {
                this.removeFile(file);
            });


            this.on('removedfile', function (file) {

                // file was sent
                if (!_.isUndefined(file.xhr)) {
                    console.log("KHAAAN!");
                    var imageUrl = JSON.parse(file.xhr.response).url;
                    removeImage(imageUrl)
                        .done(function (resp) {
                            opts.value.remove(imageUrl);
                        });
                    return;
                }

                // file wasn't sent, so it can be removed after removing it from observable
                // via a filter.

                opts.value(_.filter(opts.value(), (item) => {
                    return file.serverUrl !== item;
                }));

            })
        };

        var defaultOptions = _.extend({
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            init: dropzoneInit,
            maxFilesize: 2,
            maxFiles: opts.maxFiles,
            headers: {
                'X-CSRF-TOKEN': global.CSRF_TOKEN
            }
        }, opts);


        defaultOptions.dictDefaultMessage = "Click here or Drag & Drop Files";

        var dropzone = new Dropzone(el, defaultOptions);
        return dropzone;


    }
}

