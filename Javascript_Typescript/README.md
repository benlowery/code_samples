## Javascript Samples

[CelebrityController](CelebrityController/)

These are from an ongoing project I'm upgrading to TypeScript
BaseViewModel.js is included for completeness - its nice how interoperable TypeScript and JavaScript are.

[get-sap-calculation.js](get-sap-calculation.js)

This is the client side implementation of the SAP calc covered in the PHP directory.

It's an unusually large MVVM implementation due to the complexity of the calculation
it implements a lot of the heavy lifting server side against the Solar Thermal API.

