/*
 This implements the modified MCS SAP implementation.
 Variables bound to observables are mapped to their relevant
 h1 values in the calculation P74/P75 SAP2012 9.92
 This ensures that the calculation follows the same sequence.
 */

var Person = function (full_time, days_resident) {
    var self = this;

    self.full_time = ko.observable(full_time);
    self.days_resident = ko.observable(days_resident);

    self.full_time_equivalent = ko.computed(function () {
        return self.days_resident() / 365;
    });

    self.full_time.subscribe(function (new_value) {

        if (new_value === true) {
            self.days_resident(365);
        }
    })

};

var SapThermalCalculation = function (initData) {


    var self = this;

    self.collector_manufacturer = ko.observable().extend({required: true});
    self.collector_model = ko.observable().extend({required: true});


    self.controller_manufacturer = ko.observable().extend({required: true});

    self.controller_model = ko.observable().extend({required: true});


    // Apperture Area
    self.h1 = ko.observable();

    // Boolean - Evacuated Tube
    self.evacuated_tube = ko.observable().extend({required: true});

    /********** For Evacuated Tube Only **********/
    self.transverse_factor = ko.observable(null).extend({
        required: {
            onlyIf: function () {
                return self.evacuated_tube() === true;
            }
        }
    });

    self.longitudinal_factor = ko.observable(null).extend({
            required: {
                onlyIf: function () {
                    return self.evacuated_tube() === true;
                }
            }
        }
    );
    /********** END **********/

    /*
     IAM Only applies to evacuated tube, 1 or IAM whichever is
     Higher
     */

    self.iam_modification_factor = ko.computed(function () {
        var factor = self.transverse_factor() * self.longitudinal_factor();

        if (factor < 1) {
            return 1
        }
        else {
            return factor;
        }
    });

    self.total_aperture = ko.observable().extend({required: true, number: true});
    self.n0 = ko.observable().extend({required: true, number: true});
    self.a1 = ko.observable().extend({required: true, number: true});
    self.a2 = ko.observable().extend({required: true, number: true});

    self.postcode = ko.observable();
    self.region = ko.observable().extend({required: true});
    self.orientation = ko.observable().extend({required: true, number: true, min: 90, max: 270});
    self.tilt = ko.observable().extend({required: true, number: true, min: 0, max: 90});

    self.occupied = ko.observable().extend({required: true});

    self.peopleArray = ko.observableArray([]);

    self.calculatedOccupancy = ko.computed(function () {
        var occupancy = 0;
        $.each(self.peopleArray(), function () {
            occupancy += this.full_time_equivalent();
        });

        if (occupancy > 6) {
            return 6;
        }

        return occupancy;
    });

    self.building_area = ko.observable().extend({
        required: {
            onlyIf: function () {
                return self.occupied() === false
            }
        }
    });

    self.estimatedOccupancy = ko.observable();

    ko.computed(function () {

        $.ajax("/api/v1/thermal/sap2012/occupancy", {
            data: {area: self.building_area()},
            success: self.estimatedOccupancy
        }, self);

    });

    // We use a computed observable to update h5 whenever region, orientation
    // or tilt change, this is then passed to h5, note rateLimit to keep req count down
    ko.computed(function () {

        if (self.region() && self.orientation() && self.tilt()) {
            $.ajax("/api/v1/thermal/sap2012/annualsolar", {
                context: self,
                data: {
                    'region': self.region(),
                    'orientation': self.orientation,
                    'tilt': self.tilt
                },
                success: function (result) {
                    this.h5(result.adjusted_total);
                }
            });
        }

    }).extend({rateLimit: 500});


    // H-Values from the SAP Manual and for Display for Verification

    self.h1 = ko.computed(function () {
        return self.total_aperture();
    });

    self.h2 = ko.computed(function () {
        return self.n0();
    });

    self.h3 = ko.computed(function () {
        return self.a1();
    });

    self.h3a = ko.computed(function () {
        return self.a2();
    });

    self.h3b = ko.computed(function () {

        var a1 = parseFloat(self.a1());
        var a2 = parseFloat(self.a2());

        return 0.892 * (a1 + (a2 * 45));

    });

    self.h4 = ko.computed(function () {

        return self.h3b() / self.h2();

    });

    self.h5 = ko.observable();

    self.shadingOptions = [
        {text: "Heavy (>80%)", value: 0.5},
        {text: "Significant (>60% - 80%)", value: 0.65},
        {text: "Modest (20% - 60%)", value: 0.8},
        {text: "None/Little (<20%)", value: 1.0}
    ];

    self.panel_shading = ko.observable().extend({required: true});

    self.h6 = ko.computed(function () {
        return self.panel_shading();
    });

    self.showerOptions = [
        {text: "Non-Electric Shower(s)", value: 1.29},
        {text: "Electric Shower(s) Only", value: 0.64},
        {text: "Both Electric and Non-Electric Showers", value: 1.00},
        {text: "No Shower, Bath Only", value: 1.09}
    ];

    // h1, Page 5, MIS3001
    self.shower_type = ko.observable().extend({required: true});

    // h2, Page 5, MIS3001
    self.efficiencyOptions = [
        {text: "Water Efficent (Approved Doc G)", value: 0.95},
        {text: "Other Dwelling", value: 1}
    ];
    self.water_efficiency = ko.observable().extend({required: true});

    self.wwhrs = ko.observable(1);

    self.h7a = ko.computed(function () {
        // adjustment factor, Page 5, WIS3001
        return self.shower_type() * self.water_efficiency() * self.wwhrs();
    });

    self.c_value = ko.observable();


    self.h7 = ko.computed(function () {

        if (self.evacuated_tube() === true) {
            return self.h1() * self.h2() * self.h5() * self.h6() * self.iam_modification_factor();
        }
        else {
            return self.h1() * self.h2() * self.h5() * self.h6();
        }

    });


    self.h14 = ko.computed(function () {

        var occupancy = null;

        if (self.occupied() === false) {
            occupancy = self.estimatedOccupancy();
        }
        else {
            occupancy = self.calculatedOccupancy();
        }

        if (occupancy === 0) {
            return 0
        }
        else {
            return ((25 * occupancy + 36));
        }


    });

    self.e45m = ko.computed(function () {
        return 4.190 * self.h14() * 365 * 37 / 3600;
    });

    self.h8 = ko.computed(function () {

        return self.h7() / ((self.h7a() * self.e45m()) + self.c_value());
    });

    self.h9 = ko.computed(function () {
        if (self.h8() > 0) {
            return 1 - Math.exp(-1 / self.h8());
        }
        else {
            return 0;
        }
    });

    self.h10 = ko.computed(function () {


        if (self.h4() < 20) {

            return (0.97 - (0.0367 * self.h4()) + (0.0006 * (Math.pow(self.h4(), 2) )));

        }
        else {
            return (0.693 - 0.0108 * self.h4());
        }

    });

    self.h11 = ko.observable().extend({required: true});
    self.h12 = ko.observable().extend({required: true});

    self.h13 = ko.computed(function () {

        var h11 = parseFloat(self.h11());
        var h12 = parseFloat(self.h12());

        if (h12 > 0) {
            return (h11 + (0.3 * (h12 - h11)));
        }
        else {
            return self.h11();
        }

    });

    self.h15 = ko.computed(function () {
        return self.h13() / self.h14();
    });

    self.h16 = ko.computed(function () {
        var a = 1 + (0.2 * Math.log(self.h15()));

        if (a > 1) {
            return 1;
        }
        else {
            return a;
        }

    });

    self.h17 = ko.computed(function () {
        return self.h7() * self.h9() * self.h10() * self.h16();
    });

    ko.computed(function () {


        $.ajax("/api/v1/thermal/sap2012/cvalue", {
            context: self,
            data: {
                'volume': self.h12()
            },
            success: function (result) {
                self.c_value(result);
            }
        });

    });

    self.cylinder_manufacturer = ko.observable();
    self.cylinder_model = ko.observable();

    // Defined in standard otherwise would load from DB
    self.boilerTypes = [
        // Regular/System Boilers
        {id: 1, value: 0.768, text: "Gas, Post 1998, Condensing with Automatic Ignition"},
        {id: 2, value: 0.668, text: "Gas, Post 1998, Non-Condensing with Automatic Ignition"},
        {id: 3, value: 0.618, text: "Gas, Pre-1998, Fan Flue"},
        {id: 4, value: 0.588, text: "Gas, 1979 - 1997, Open or Balanced Flue, Floor Mounted"},
        {id: 5, value: 0.488, text: "Gas, Pre-1979, Open or Balanced Flue, Floor Mounted"},
        {id: 6, value: 0.754, text: "Oil, Condensing"},
        {id: 7, value: 0.714, text: "Oil, Standard, Post 1998"},
        {id: 8, value: 0.624, text: "Oil, Standard, 1985-1997"},
        {id: 9, value: 0.574, text: "Oil, Standard, Pre-1985"},
        // Combi Boilers
        {id: 10, value: 0.775, text: "Gas, Post 1998, Condensing with Automatic Ignition"},
        {id: 11, value: 0.675, text: "Gas, Post 1998, Non-Condensing with Automatic Ignition"},
        {id: 12, value: 0.645, text: "Gas, Pre 1998, Fan Flue"},
        {id: 13, value: 0.645, text: "Oil, Pre 1998"},
        {id: 14, value: 0.705, text: "Oil, Post 1998"},
        // Other/Backup Heaters
        {id: 15, value: 0.630, text: "Wood Chip/Pellet Independent Boiler"},
        {id: 16, value: 0.650, text: "Closed Room Heater with Boiler Radiator"},
        {id: 17, value: 0.550, text: "Manual Feed/Independent Boiler"},
        {id: 18, value: 1.00, text: "Immersion Heater with DHW"},
        {id: 19, value: 0.85, text: "Electric Boiler Separate from DHW Tank"},
        {id: 20, value: 1.50, text: "Ground to Water Heat Pump"},
        {id: 21, value: 1.43, text: "Air to Water Heat Pump"}

    ];

    self.boiler_type = ko.observable().extend({required: true});

    self.fuel_saved = ko.computed(function () {
        var boiler = _.find(self.boilerTypes, function (item) {
            if (item.id === self.boiler_type()) {
                return true;
            }
        });

        if (!_.isUndefined(boiler)) {
            return self.h17() / boiler.value;
        }


    });

    self.fuelOptions = ['Gas', 'Electricity', 'Oil', 'Coal', 'Wood', 'Other'];
    self.fuel_type = ko.observable();

    ko.computed(function () {

        var evacuated_tube = self.evacuated_tube();
        if (evacuated_tube === false) {
            self.transverse_factor(null);
            self.transverse_factor.isModified(false);
            self.longitudinal_factor(null);
            self.longitudinal_factor.isModified(false);
        }

    });


    ko.computed(function () {

        var occupied = self.occupied();

        if (occupied === false) {
            self.peopleArray.removeAll();
        }

    });

    // Methods

    self.initialise = function (sap_calc) {


        self.collector_manufacturer(sap_calc.json_data.collector_manufacturer);
        self.collector_model(sap_calc.json_data.collector_model);
        self.controller_manufacturer(sap_calc.json_data.controller_manufacturer);
        self.controller_model(sap_calc.json_data.controller_model);

        self.evacuated_tube(sap_calc.json_data.evacuated_tube);
        self.transverse_factor(sap_calc.json_data.transverse_factor);
        self.longitudinal_factor(sap_calc.json_data.longitudinal_factor);
        self.total_aperture(sap_calc.json_data.total_aperture);
        self.n0(sap_calc.json_data.n0);
        self.a1(sap_calc.json_data.a1);
        self.a2(sap_calc.json_data.a2);
        self.panel_shading(sap_calc.json_data.panel_shading);
        self.postcode(sap_calc.json_data.postcode);
        self.region(sap_calc.json_data.region);
        self.orientation(sap_calc.json_data.orientation);
        self.tilt(sap_calc.json_data.tilt);
        self.shower_type(sap_calc.json_data.shower_type);
        self.water_efficiency(sap_calc.json_data.water_efficiency);

        self.cylinder_manufacturer(sap_calc.json_data.cylinder_manufacturer);
        self.cylinder_model(sap_calc.json_data.cylinder_model);

        self.boiler_type(sap_calc.json_data.boiler_type);
        self.fuel_type(sap_calc.json_data.fuel_type);
        self.h12(sap_calc.json_data.h12);
        self.h11(sap_calc.json_data.h11);
        self.occupied(sap_calc.json_data.occupied);


        if (sap_calc.json_data.occupied) {

            _.each(sap_calc.json_data.peopleArray, function (person) {

                self.peopleArray.push(new Person(person.full_time, person.days_resident));
            })

        }
        else {
            self.building_area(sap_calc.json_data.building_area);
        }

    };

    self.lookupRegion = function () {
        if (self.postcode()) {
            $.ajax("/api/v1/thermal/sap2012/region", {
                data: {postcode: self.postcode()},
                success: self.region
            }, self);
        }
    };

    self.addPerson = function () {
        self.peopleArray.push(new Person(true, 365));
    };

    self.removePerson = function (person) {

        if (self.peopleArray().length > 1) {
            self.peopleArray.remove(person);
        }

    };

    self.clientErrors = ko.validatedObservable(_.omit(self, ['clientErrors']));

    self.onSubmit = function () {

        if (!self.ignoreClientErrors) {
            if (!self.clientErrors.isValid()) {
                self.clientErrors.errors.showAllMessages();
                return false;
            }
        }

        var formData = self.convertRemove([
            'boilerTypes', 'efficiencyOptions', 'shadingOptions', 'showerOptions'
        ]);


        $.ajax({
            type: 'POST',
            url: initData.url,
            data: formData,
            contentType: "application/json",
            success: self.successHandler,
            error: self.errorHandler
        });


    };

    if (!_.isNull(initData.sap_calc)) {
        self.initialise(initData.sap_calc);
    }
    else {

    }

    BaseViewModel.call(this);
};

var sapThermalCalculation = new SapThermalCalculation(initData);
ko.applyBindings(sapThermalCalculation);
