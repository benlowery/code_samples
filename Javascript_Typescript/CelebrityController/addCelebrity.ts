require('main.js');

require('lib/knockout-dropzone.js');
require('lib/knockout-ckeditor.js');

declare var initData: any; // as initData is passed down and bound externally

import BaseViewModel  = require('lib/BaseViewModel');

import domready = require('domready');
import _ = require('lodash');
import ui = require('lib/ui.js');

// initData interface, as this is an add it only contains one property the submit url
interface InitData {
    url: string;
}

// load default page infrastructure
ui.loadUI();

domready(function () {
    class AddCelebrity extends BaseViewModel {

        private _data: InitData;
        private celebrity_first_name: KnockoutObservable<string>;
        private celebrity_last_name: KnockoutObservable<string>;
        private primary_image: KnockoutObservableArray<any>;
        private celebrity_description: KnockoutObservable<string>;
        public clientErrors: any; // any because not TS class/object as parent

        constructor(data: InitData) {
            super();
            this._data = data;
            this.celebrity_first_name = ko.observable('').extend({required: {message: 'You must enter Celebrity first name'}});
            this.celebrity_last_name = ko.observable('').extend({required: {message: 'You must enter a Celebrity surname'}});
            this.primary_image = ko.observableArray([]).extend({required: {message: 'A Primary image for a Celebrity is required'}});
            this.celebrity_description = ko.observable('').extend({required: {message: 'Please enter a description for the Celebrity'}});

            this.clientErrors = ko.validatedObservable(_.omit(this, ['clientErrors', 'messages', 'growls', '_data']));
        }

        onSubmit() {

            if (!this.clientErrors.isValid()) {
                this.setGrowlMessage("There are errors in your data, please check your entries!", 'warning');
                this.clientErrors.errors.showAllMessages(true);

                return
            }

            this.postData(this._data.url);

        }
    }

    var addCelebrity = new AddCelebrity(initData);
    ko.applyBindings(addCelebrity);

});
