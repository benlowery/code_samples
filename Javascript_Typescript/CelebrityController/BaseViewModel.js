var ko = require('knockout');
var $ = require('jquery');
var _ = require('lodash');
var swal = require('sweetalert');
var notifify = require('bootstrap-notify');
var pubsub = require('pubsub-js');

/**
 * This is a page BaseViewModel, it should be extended and never used directly
 *
 * It also sets up the various subscribers that components can fire (see pubsub-js).
 *
 * It also understands the extended validation format used in Meta
 */


class BaseViewModel {
    constructor() {

        this.messages = ko.observableArray([]);
        this.growls = ko.observableArray([]);

        // this is the global "validator" that is set but not initalised
        // subclasses need to pass an array to this validator validatedObservable(['properties','etc']);
        this.clientErrors = ko.validatedObservable([]);
        pubsub.subscribe('com', this.comSlot.bind(this));
        pubsub.subscribe('com.page', this.pageSlot.bind(this));
        pubsub.subscribe('com.page.growl', this.growlMessageSlot.bind(this));
        pubsub.subscribe('com.page.message', this.pageMessageSlot.bind(this));
        pubsub.subscribe('com.page.delete_message', this.deleteMessageSlot.bind(this));
    }

    pageMessageSlot(signal, msg) {
        this.setPageMessage(msg.message, msg.type);
    }

    growlMessageSlot(signal, growl) {
        this.setGrowlMessage(growl.message, growl.type);
    }

    sweetAlert(config) {
        swal(config);
    }

    postData(url, data = null) {
        if (_.isUndefined(url)) {
            console.log("No URL Specified");
            return;
        }

        var sendData;
        if (_.isNull(data)) {
            sendData = this.jsonify();
        }
        else {
            sendData = ko.toJSON(data);
        }

        // note use of "context: this" otherwise you'd have to do this.onSuccessPosting.bind(this) everywhere
        $.ajax({
            url: url,
            method: 'POST',
            data: sendData,
            contentType: "application/json",
            context: this,
            success: this.handleSuccess,
            error: this.handleError
        });
    }

    deleteMessageSlot(signal, message) {
        this.messages.remove(message)
    }

    prepareForSend(removeDefaults = true, furtherProperties = []) {
        // create clean copy
        var copy = ko.toJS(this);
        // remove default properties we don't need to send

        _.forOwn(copy, (value, key) => {

            if (key.charAt(0) === "_") {
                delete copy[key];
            }
        });

        if (removeDefaults === true) {
            delete copy.messages;
            delete copy.growls;
            delete copy.clientErrors;
            delete copy.__proto__;
            delete copy._data;
        }

        _.each(furtherProperties, function (property) {
            delete copy[property];
        });

        return copy;
    }

    jsonify(removeDefaults = true, furtherProperties = []) {
        var copy = this.prepareForSend(removeDefaults, furtherProperties);
        return ko.toJSON(copy);
    }

    debugify(removeDefaults = true, furtherProperties = []) {
        var copy = this.prepareForSend(removeDefaults, furtherProperties);
        return ko.toJSON(copy, null, 4);
    }

    keyify(removeDefaults = true, furtherProperties = []) {
        var copy = this.prepareForSend(removeDefaults, furtherProperties);
        return ko.toJSON(_.keys(copy), null, 4);
    }

    handleSuccess(response, statusText, xhr) {

        // without a status we don't really know what to do so just return and don't do
        // anything
        if (!response.status) {

            return;
        }

        if (response.status === "redirect") {
            this.redirect(response.payload.url);
        }

        if (_.isArray(response.payload.messages) && _.size(response.payload.messages) > 0) {
            this.handleMessages(response);
        }

        if (typeof this.handleSuccessResponse === "function") {
            this.handleSuccessResponse(response, statusText, xhr);
        }
    }

    redirect(url) {

        window.location.href = url;
    }

    handleError(response, statusText, xhr) {

        var responseObj = response.responseJSON;


        if (_.isArray(responseObj.payload.messages) && _.size(responseObj.payload.messages) > 0) {
            this.handleMessages(responseObj);
        }

        if (typeof responseObj.payload.formerrors !== 'undefined') {
            this.handleFormErrors(responseObj.payload.formerrors);
        }

        // finally check if "descendant" has handleErrorResponse
        if (typeof this.handleErrorResponse === "function") {
            this.handleErrorResponse(response, statusText, xhr)
        }
    }

    // convenience method that wraps arguments as object
    setGrowlMessage(message, type) {
        this.setGrowlMessages({message: message, type: type});
    }

    // Growl Style Message
    setGrowlMessages(messages) {

        if (!_.isArray(messages)) {
            messages = [messages];
        }

        _.forEach(messages, (message) => {
            $.notify({
                    title: "",
                    message: message.message
                },
                {
                    type: message.type,
                    delay: 5000,
                    newest_on_top: false,
                    template: '<div data-notify="container" class="col-xs-18 col-sm-12 col-lg-8 alert alert-{0} notification-container" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<h4><span data-notify="message">{2}</span></h4>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
                });
        });


    }

    setPageMessage(message, type) {
        this.setPageMessages({message: message, type: type});
    }

    setPageMessages(messages) {
        this.clearPageMessages();

        if (!_.isArray(messages)) {
            messages = [messages];
        }

        _.forEach(messages, (message) => {
            if (message.type === undefined) {
                message.type = "info";
            }
            this.messages.push(message);
        });

    }

    handleMessages(response) {


        var growl_messages = _.filter(response.payload.messages, (message) => {

            return message.displayAs === "growl";
        });

        var page_messages = _.filter(response.payload.messages, (message) => {
            return message.displayAs !== "growl";
        });


        this.setGrowlMessages(growl_messages);
        this.setPageMessages(page_messages);
    }

    clearPageMessages() {
        this.messages([]);
    }

    clearExistingFormErrors() {
        for (var property in this) {
            // property belongs to *this* and is observable
            if (this.hasOwnProperty(property) && ko.isObservable(this[property])) {

                // if it has clearError
                if (!_.isUndefined(this[property].clearError)) {
                    // clear local errors as we only care about remote errors
                    this[property].clearError();
                }

            }
        }
    }

    handleFormErrors(formerrors) {

        // Remove existing errors
        this.clearExistingFormErrors();


        for (var property in formerrors) {

            var field_array = property.split('.');

            if (field_array[0] === "*") {

                this.setPageMessages(_.map(formerrors[property], (errorMsg) => {
                    return {message: errorMsg, type: "warning"}
                }));

            }

            var field = field_array.reduce(function (object, index) {
                return object[index];
            }, this);

            if (ko.isObservable(field)) {
                var message = formerrors[property].join(",");
                field.setError(message);
                field.isModified(true);
            }
        }

    }

}

module.exports = BaseViewModel;
