require('main.js');

require('lib/knockout-dropzone.js');
require('lib/knockout-ckeditor.js');

// since initData is bound outside module scope
declare var initData: any;

import domready = require('domready');
import _ = require('lodash');
import ui = require('lib/ui.js');

import BaseViewModel = require('lib/BaseViewModel');

// Interface for inital data form expects URL and Celebrity
interface InitData {
    url: string;
    celebrity: any
}

ui.loadUI();

domready(function () {

    class editCelebrity extends BaseViewModel {

        private _data: InitData;
        private celebrity_first_name: KnockoutObservable<string>;
        private celebrity_last_name: KnockoutObservable<string>;
        private primary_image: KnockoutObservableArray<any>;
        private celebrity_description: KnockoutObservable<string>;
        public clientErrors: any; // any because not TS class/object as parent

        constructor(data) {
            super();

            this._data = data;

            this.celebrity_first_name = ko.observable("").extend({required: true});
            this.celebrity_last_name = ko.observable("").extend({required: true});
            this.primary_image = ko.observableArray([]).extend({required: true});
            this.celebrity_description = ko.observable("").extend({required: true});

            this.clientErrors = ko.validatedObservable(_.omit(this, ['clientErrors', 'messages', 'growls', '_data']));
        }

        map() {
            this.celebrity_first_name(this._data.celebrity.first_name);
            this.celebrity_last_name(this._data.celebrity.last_name);
            this.primary_image.push(this._data.celebrity.primary_image);
            this.celebrity_description(this._data.celebrity.description);
        }

        onSubmit() {

            if (!this.clientErrors.isValid()) {
                this.setGrowlMessage("There are errors in your data, please check your entries", 'warning');
                this.clientErrors.errors.showAllMessages(true);
                return
            }

            this.postData(this._data.url);
        }
    }

    var addCelebrity = new editCelebrity(initData);
    addCelebrity.map();
    ko.applyBindings(addCelebrity);

});
