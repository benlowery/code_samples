declare var initData: any;

require('main.js');
require('lib/knockout-dropzone.js');
require('lib/knockout-datatables.js');

import swal = require('sweetalert');
import domready = require('domready');
import _ = require('lodash');

import ui = require('lib/ui.js');
import BaseViewModel = require('lib/BaseViewModel');

// If you bind without creating a class you can do things much simpler
// this.tableData = ko.observableArray(initData.celebrities)
// if you do this you don't need to put () on the end.

ui.loadUI();

class Celebrity {

    public id: KnockoutObservable<string>;
    public first_name: KnockoutObservable<string>;
    public last_name: KnockoutObservable<string>;
    public uuid: KnockoutObservable<string>;
    public editLink: KnockoutObservable<string>;
    public cartLink: KnockoutObservable<string>;
    public newCartPostLink: KnockoutObservable<string>;

    constructor(data) {
        this.id = ko.observable(data.id);
        this.first_name = ko.observable(data.first_name);
        this.last_name = ko.observable(data.last_name);
        this.uuid = ko.observable(data.uuid);

        this.editLink = ko.observable(`/admin/celebrity/${data.id}/edit`);
        this.cartLink = ko.observable(`/admin/celebrity/${data.id}/cmc-posts`);
        this.newCartPostLink = ko.observable(`/admin/celebrity/${data.id}/cmc-post/add`);

    }
}

class CelebritiesOverview extends BaseViewModel {

    public tableData: any;

    constructor(initData) {
        super();

        this.tableData = ko.observableArray(_.map(initData.celebrities, function (item) {
            return new Celebrity(item);
        }));

    }

    onDeleteCelebrityClicked(item) {
        swal({
            title: "",
            text: `Are you sure you want to remove ${item.first_name()} ${item.last_name()}`,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete them!",
            closeOnConfirm: false
        }, function () {
            swal("Done!", `${item.first_name()} ${item.last_name()}`, "success");
        });
    }

}

domready(function () {

    var celebritiesOverview = new CelebritiesOverview(initData);
    ko.applyBindings(celebritiesOverview);


});
