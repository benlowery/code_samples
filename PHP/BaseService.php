<?php

namespace Meta\Utility\Base;

use Illuminate\Support\MessageBag;

class BaseService
{
    public $messages;

    public function __construct()

    {
        $this->errors = new MessageBag();
        $this->messages = new ClientMessages();
    }

    public function setErrors($errors): void
    {
        $this->errors->merge($errors);
    }

    public function setMessage($message, $type = MessageType::INFO, $displayAs = DisplayAs::PAGE): void
    {
        $this->messages->add($message, $type, $displayAs);
    }

    public function setMessages($messages): void
    {
        $this->messages->merge($messages);
    }

    public function formHasErrors(): bool
    {
        return count($this->errors) > 0;
    }
}
