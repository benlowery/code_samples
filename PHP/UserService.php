<?php

namespace Meta\Core\User;

use Cartalyst\Sentinel\Sentinel;
use Illuminate\Support\Collection;

use Meta\Admin\Validators\EmployeeValidator;
use Meta\Utility\Base\BaseService;
use Meta\Utility\Base\DisplayAs;
use Meta\Utility\Base\MessageType;

// User Service is for things that affect the users,
// DON'T PUT PERMISSION CHECKS HERE - SEE Permissions.php

/**
 * Class UserService
 * @package Meta\Core\User
 */
class UserService extends BaseService
{
    private $sentinel;
    private $userRepository;
    private $roleRepository;
    private $model;
    private $employeeValidator;

    public function __construct(Sentinel $sentinel, User $user, EmployeeValidator $employeeValidator)
    {
        parent::__construct();
        $this->sentinel = $sentinel;
        $this->userRepository = $this->sentinel->getUserRepository();
        $this->roleRepository = $this->sentinel->getRoleRepository();
        $this->model = $user;

        $this->employeeValidator = $employeeValidator;
    }

    public function userLogin(array $data)
    {

        $credentials = [
            'email'    => $data['identifier'],
            'password' => $data['password'],
        ];

        return $this->sentinel->authenticate($credentials);
    }

    public function logOut()
    {
        return $this->sentinel->logout();
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roleRepository = $this->sentinel->getRoleRepository();

        return $roleRepository->get();
    }

    /**
     * Retrieve all users with related role information as an array
     *
     * @return array|null
     * @throws \Exception
     */
    public function getEmployeeOverview(): ?array
    {

        $managerRoleId = $this->roleRepository->findBySlug('manager')->id;
        $cartUserRoleId = $this->roleRepository->findBySlug('cart-user')->id;

        if (is_null($managerRoleId) || is_null($cartUserRoleId)) {
            throw new \Exception('Unable to find one of the Employee roles');
        }

        $pdo = \DB::connection()->getPdo();

        $stmt = $pdo->prepare(
            "
            SELECT
              users.id                     AS id,
              users.first_name             AS first_name,
              users.last_name              AS last_name,
              users.email                  AS email,
              string_agg(roles.name, ', ') AS combined_roles
            FROM users
              INNER JOIN role_users ON users.id = role_users.user_id
              INNER JOIN roles ON role_users.role_id = roles.id AND role_users.role_id IN (
                :managerRoleId,
                :cartUserRoleId
              )
            GROUP BY users.id;
            "
        );

        $stmt->execute(compact('cartUserRoleId', 'managerRoleId'));

        return $stmt->fetchAll(\PDO::FETCH_CLASS);

    }

    /**
     * Add New User
     *
     * @param array $data
     * @return User|null
     */
    public function addNewEmployee(array $data): ?User
    {
        $c = collect($data);

        if (!$this->employeeValidator->passes('create')) {
            $this->setErrors($this->employeeValidator->errors());
            $this->setMessage('There are errors with your form - please check and try again', MessageType::DANGER);

            return null;
        }

        \DB::beginTransaction();

        try {
            $newUser = $this->userCreate($c, $c->get('chosen_role_id'));

            // if we have a new user then return it otherwise
            if ($newUser) {
                \DB::commit();

                return $newUser;
            }

            return null;
        } catch (\Exception $e) {
            $this->setMessage('foo');
            $this->setMessages($this->userService->messages);
            \DB::rollBack();

            return null;
        }

    }

    public function userCreate(Collection $data, $roleId): ?User
    {
        $role = $this->roleRepository->findById($roleId);
        if (is_null($role)) {
            throw new \Exception('Unable to find a role with that ID');
        }

        $credentials = [
            'email'      => $data->get('email_address', null),
            'password'   => $data->get('password', null),
            'first_name' => $data->get('first_name', null),
            'last_name'  => $data->get('last_name', null),
        ];

        if (!$this->userRepository->validForCreation($credentials)
        ) {
            throw new \Exception('Unable to create user with those credentials');
        }

        if (!$this->model::where('email', '=', $data->get('email_address'))->get()->isEmpty()) {
            $this->setMessage('A user already exists with email address '.$data->get('email_address'), MessageType::DANGER);

            return null;
        }

        $user = $this->userRepository->create($credentials);

        $role->users()->attach($user);

        return $user;
    }

    /**
     * Update existing employee
     *
     * @param $userId
     * @param array $data
     * @return User|null
     */
    public function updateEmployee($userId, array $data): ?User
    {
        $c = collect($data);

        $user = $this->getUserById($userId);
        $newRole = $this->roleRepository->findById($c->get('chosen_role_id'));

        if (!$this->employeeValidator->passes('update')) {
            $this->setErrors($this->employeeValidator->errors());
            $this->setMessage('There are errors with your form - please check and try again', MessageType::DANGER);

            return null;
        }


        $credentials = [
            'email'      => $c->get('email_address', null),
            'first_name' => $c->get('first_name', null),
            'last_name'  => $c->get('last_name', null),
        ];

        if ($c->get('password')) {
            $credentials['password'] = $c->get('password');
        }

        // loop over and remove all roles on user
        foreach ($user->roles as $role) {
            $role->users()->detach($user);
        }

        $newRole->users()->attach($user);

        $user = $this->userRepository->update($user, $credentials);

        return $user;

    }

    /**
     * Get existing user by ID.
     *
     * @param $userId
     * @return User|null
     */
    public function getUserById($userId): ?User
    {
        return $this->model::find($userId);
    }

    /**
     * Delete existing employee
     *
     * @param $employeeId
     * @return bool
     */
    public function deleteEmployee($employeeId): bool
    {

        $user = $this->getUserById($employeeId);

        if (is_null($user)) {
            $this->setMessage("User with ID: $employeeId doesn't exist", MessageType::WARNING, DisplayAs::GROWL);

            return false;
        }

        foreach ($user->roles as $role) {
            $role->users()->detach($user);
        }

        return $user->delete();

    }


}
