<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\MessageBag;
use Meta\Core\User\Permissions;
use Meta\Core\Exceptions\InvalidPermissionException;
use Meta\Core\Exceptions\AccessConditionFailureException;

class AccessCheck
{
    /**
     * @var \Illuminate\Support\MessageBag
     */
    public $messages;

    public function __construct(Permissions $permissions)
    {
        $this->messages = new MessageBag();
        $this->permissions = $permissions;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next, ...$params)
    {

        // If there are no permissions just pass straight through
        if (null === $this->getPermissions($request)) {
            return $next($request);
        }

        $allowed = $this->allowed($request);

        if (!$allowed) {

            if (!$this->permissions->userIsAuthenticated()) {
                return redirect('admin/login');
            }

            throw new AccessConditionFailureException('Access Denied');
            //return redirect('403')->with('messages', $this->messages->toArray());
        }

        return $next($request);
    }

    /**
     * returns the permission string i.e. is:root|can:do.something
     * e.g. group(['permissions' => 'is:manager,cart-user|can:some.permission'] ...
     *
     * @param $request
     * @return array|null
     */
    public function getPermissions($request): ?array
    {

        $actions = $request->route()->getAction();

        if (isset($actions['permissions'])) {
            return $actions['permissions'];
        }

        return null;
    }

    /**
     * Determine if the AccessCheck should allow the request
     * to proceed.
     *
     * @param $request
     * @return bool|null
     */
    public function allowed($request): ?bool
    {
        $permissions = $this->getPermissions($request);

        $allowed = $this->checkAllConditions($permissions);

        if (null === $permissions) {
            return null;
        }

        return $allowed;
    }

    /**
     * Check all conditions are valid and pass
     *
     * @param $conditionString
     * @return bool
     */
    public function checkAllConditions($conditionString): bool
    {

        if ($conditionString === '') {
            return true;
        }

        // split on the | so that we get an array of separate conditions
        $conditions = $this->explodeCondition($conditionString);

        // We assume that a permission is true (pass through model) unless something
        // violates the conditions in permission conditions
        $isFlag = true;
        $isNotFlag = true;
        $canFlag = true;
        $canNotFlag = true;
        $isAuthenticated = true;

        foreach ($conditions as $condition) {

            // split condition into name (i.e. is,is_not) and values (is:root,engineer becomes ['root','engineer']
            $parsedCondition = $this->parseCondition($condition);

            switch ($parsedCondition['name']) {
                case 'is':

                    $isFlag = $this->validateIsCondition($parsedCondition['values']);
                    break;
                case 'is_not':
                    $isNotFlag = $this->validateIsNotCondition($parsedCondition['values']);
                    break;
                case 'can':
                    $canFlag = $this->validateCanCondition($parsedCondition['values']);
                    break;
                case 'can_not':
                    $canNotFlag = $this->validateCanNotCondition($parsedCondition['values']);
                    break;
                case 'auth':
                    $isAuthenticated = $this->validateAuthenticatedCondition($parsedCondition['values']);
                    break;
                default:
            }
        }

        return $isFlag & $isNotFlag & $canFlag & $canNotFlag & $isAuthenticated;

    }

    /**
     * Explode conditions on | separator
     *
     * @param $conditions
     * @return array
     */

    public function explodeCondition($conditions): array
    {
        return is_string($conditions) ? explode('|', $conditions) : $conditions;
    }

    public function parseCondition($condition): array
    {
        if (strpos($condition, ':') === false) {
            throw new InvalidPermissionException('Unable to parse Permission Condition');
        }

        $parsed = explode(':', $condition, 2);

        $name = $parsed[0];
        $values = str_getcsv($parsed[1]);

        return ['name' => $name, 'values' => $values];
    }

    /**
     * Check isCondition is true, append fail messages if not
     *
     * @param array $values
     * @return bool
     */
    public function validateIsCondition(array $values): bool
    {

        foreach ($values as $value) {
            if ($this->permissions->is($value)) {
                return true;
            }
        }

        $this->messages->add('failed_conditions', 'You lack one of the required roles');
        $this->messages->add('failed_conditions', $values);

        return false;
    }

    /**
     * Check isNotCondition, append fail messages if not
     *
     * @param array $values
     * @return bool
     */
    public function validateIsNotCondition(array $values): bool
    {

        foreach ($values as $value) {
            if ($this->permissions->is($value)) {
                $this->messages->add(
                    'failed_conditions',
                    "You have role $value and don't have access to this resource"
                );

                return false;
            }
        }

        return true;
    }

    /**
     * Validate Can condition, append fail messages if not
     *
     * @param array $values
     * @return bool
     */
    public function validateCanCondition(array $values): bool
    {
        foreach ($values as $value) {

            if (!$this->permissions->can($value)) {
                $this->messages->add(
                    'failed_conditions',
                    'You lack one of the required permissions to access this resource'
                );

                return false;
            }
        }

        return true;

    }

    /**
     * Validate CanNot condition, append fail messages if not
     *
     * @param array $values
     * @return bool
     */
    public function validateCanNotCondition(array $values): bool
    {
        foreach ($values as $value) {
            if ($this->permissions->can($value)) {
                $this->messages->add('failed_conditions', 'Failed can_not check');

                return false;
            }
        }

        return true;
    }

    /**
     * Validate Auth condition
     *
     * @param array $values
     * @return bool
     */
    public function validateAuthenticatedCondition(array $values): bool
    {
        $condition = $values[0];

        // if auth yes||true and logged in return true (requires and checks that the user is auth'd)
        return ($condition === 'yes' || $condition === 'true') && $this->permissions->userIsAuthenticated();
    }
}
