# PHP Samples

I wasn't sure what to put in here and it was complicated by having to ask permission from clients
to include code samples that had copyright assigned to them.

## SolarThermal2012

----------

[SolarThermal2012.php](SolarThermal2012.php)

You can see a development mode demo of the overall project [here on youtube](https://youtu.be/-cL2m1UXp0g).

I was the sole front/backend developer on this
project, all the PDF generation was done with a toolkit I built myself by beating up bootstrap and altering it to generate nice looking printable/client quality PDF's, the PDF illustrated
at 3:43 in the video was generated from 'pure' HTML.

This was a multi-tenant/SaaS platform for managing all aspects of running an MCS certified business
it managed staff training, sub contractors, suppliers, products (including invoicing and stock allocation)
as well as Documents, Meetings (with nominations), Equipment Management and general business details.

It was also fully responsive with the intention it could be used on site by engineers.

This was an implementation of the Microgeneration Certification Scheme Solar Thermal
calculation as amended from "The Government's Standard Assessment Procedure for 
Energy Rating of Dwellings".

Actually a bit of a fun story, originally I was going to
purchase this in as an API from a third party, we bought their API which was expensive
and none of the results matched when I started testing it so I rang them and they said "Oh yeah, we are getting
getting out of that side of the business" so I had two weeks to implement Pages 76 to 79
of [this](https://www.bre.co.uk/filelibrary/SAP/2012/SAP-2012_9-92.pdf) as amended by 
[this](http://www.microgenerationcertification.org/images/MCS%20024%20Issue%201.1%20-%20Solar%20Domestic%20Hot%20Water%20Energy%20Calculation%2016.12.2013.pdf)
altogether the calculation had 40 odd separate steps with multiple nested conditionals and
required pulling in data from a dozen tables and a postcode lookup against region
for the whole of the UK plus some trigonometry I had not used in a long time!

This was a tiny part of a much larger system and you can see the calc working in real time
here [on youtube](https://youtu.be/E16YGO6Y3mg).

General stack was Laravel, PostgreSQL, KnockoutJS

----------

[MetaValidator.php](MetaValidator.php)

This was a solution to the problem of validating multiple similar cases with more complex business
logic and how to handle business edge cases.

In essence it extends the normal laravel validator except it decouples the validation from the request
allowing you to use alternative data sources, it also uses magic methods to create validation methods
for a particular rule (so a rule called foo would mean that a method called validateFoo would be called and checked)
as well as implementing a generic complexRules method as a trapdoor if needed.

Inside a server/repository usage would look like this

    if (!$this->validator->passes('create')) {
        $this->setErrors($this->validator->errors());
        $this->setMessage('There are errors with your form - please check and try again');

        return null;
    }
    
Errors can then be mapped back onto the form on the client side by sending them down as JSON or some
other method.

[AccessCheck.php](AccessCheck.php)

Many of the projects I work on require quite sophisticated role based access control systems.

Cartalyst/Sentinel is a useful package for that but it doesn't have a good way to apply and validate
access control checks at the Route layer. 

This effectively requires that you do all those checks in a controller method, that led to a lot of duplicated 
code that was hard to refactor and reason about so I wrote a middleware that extended Cartalyst/Sentinel to allow
grouping routes like so :-

    group(
        ['permissions' => 'is:manager,cart-user'],
        function () {
    
            $adminCelebrityEdit = 'admin/celebrity/{id}/edit';
            get($adminCelebrityEdit, call(CelebrityController::class, 'showEditCelebrity'))->name('celebrity_edit');
            post($adminCelebrityEdit, call(CelebrityController::class, 'postEditCelebrity'));

Note group() isn't a Laravel inbuilt method, I created my own helpers file to simplify route declaration files call() is a similar
extension that negates the 'This\Is\A\Really\Long\Namespace@someMethod' method of mapping a route to a class method,
it also allows much easier refactoring since it leveragesPHP namespaces properly, not a huge fan of magic strings.

[UserService.php](UserService.php)

UserService is a typical service out of a recent application, it extends from [BaseService.php](BaseService.php) which adds a thin
application layer over the top (mostly related to consistent application semantics using [ClientMessages](ClientMessages.php)

