<?php

namespace Meta\Utility\Validators;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as ValidatorFactory;

abstract class MetaValidator
{
    protected static $rules = null;
    protected static $messages = null;

    public $data;
    public $errors;

    public function __construct($input = null, Request $request, ValidatorFactory $validatorFactory)
    {
        $this->errors = new MessageBag(); // store errors
        // if input is null then validator assume we are validating
        // $request->all();
        $this->data = new Collection($input ?: $request->all());
        $this->validatorFactory = $validatorFactory;
    }

    public function with($input)
    {
        $this->data = new Collection($input);

        return $this;
    }

    public function fails($action = null, ...$args)
    {
        return !$this->passes($action, ...$args);
    }

    public function passes($action = null, ...$args)
    {
        if (is_null($action)) {
            throw new \Exception('Action can not be null!');
        }

        $actionResult = $this->handleAction($action, $args);
        $rulesResult = $this->handleRules($action);

        return ($rulesResult && $actionResult);
    }

    public function handleRules($action)
    {

        // if we have no rules then it's valid.
        if (is_null(static::$rules)) {
            return true;
        }

        // create a new validator via the injected factory
        $validator = $this->validatorFactory
            ->make(
                $this->data->toArray(),
                array_dot(static::$rules[$action]),
                static::$messages ?: []
            );

        if ($validator->passes()) {
            return true;
        }

        $this->errors->merge($validator->messages());

        return false;
    }

    public function handleAction($action, ...$args)
    {
        $actionMethodName = camel_case('validate'.ucfirst($action));

        // complexRules is a single method that can be applied to all
        // rules, note this steps on validate<Foo> below
        if (is_callable([$this, 'complexRules'])) {
            return $this->complexRules();
        }

        // if we have an action to validated called validate<Foo> call it
        // and return it's value
        if (is_callable([$this, $actionMethodName])) {
            return $this->$actionMethodName($args);
        }

        return true;

    }

    public function errors()
    {
        return $this->errors;
    }

    public function errorsArray()
    {
        return $this->errors->toArray();
    }

    public function setError($key, $string)
    {
        $this->errors->add($key, $string);
    }
}
