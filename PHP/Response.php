<?php

namespace Meta\Utility\Framework;

use Illuminate\Routing\ResponseFactory;
use Meta\Utility\Base\ClientMessages;
use Meta\Utility\Base\DisplayAs;
use Meta\Utility\Base\MessageType;

/**
 * Extension of Laravels standard Response to add application layer semantics
 *
 * Note that all methods return JsonResponse.
 * @package Meta\Utility\Framework
 */
class Response extends ResponseFactory
{

    /**
     * @param $url
     * @param null $message
     * @param string $type
     * @param string $displayAs
     * @return \Illuminate\Http\JsonResponse
     */
    public function redirect($url, $message = null, $type = MessageType::SUCCESS, $displayAs = DisplayAs::FLASH): \Illuminate\Http\JsonResponse
    {
        if (!is_null($message)) {
            $cm = new ClientMessages();
            $cm->add($message, $type, $displayAs);
        }

        $payload = [
            'url' => $url,
        ];

        return $this->appResponse($payload, 'redirect', 200);
    }

    /**
     * @param array $payload
     * @param $status
     * @param int $httpCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function appResponse(array $payload, $status, $httpCode = 200): \Illuminate\Http\JsonResponse
    {
        $responseData = [
            'status'  => $status,
            'payload' => $payload,
        ];

        return $this->json($responseData, $httpCode);
    }

    /**
     * @param $errors
     * @param ClientMessages[] ...$otherMessages
     * @return \Illuminate\Http\JsonResponse
     */
    public function formErrors($errors, ClientMessages ...$otherMessages): \Illuminate\Http\JsonResponse
    {

        $aggregator = array_reduce(
            $otherMessages,
            function (ClientMessages $carry, $item) {
                return $carry->merge($item);
            },
            new ClientMessages()
        );

        $payload = [
            'messages'   => $aggregator->messages(),
            'formerrors' => $errors,
        ];

        return $this->appResponse($payload, 'error', 400);

    }

    /**
     * @param $messages
     * @param string $status
     * @param int $httpCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function messages(ClientMessages $messages, $status = 'success', $httpCode = 200): \Illuminate\Http\JsonResponse
    {

        $payload = [
            'messages' => $messages->messages(),
        ];

        return $this->appResponse($payload, $status, $httpCode);
    }

    /**
     * @param $message
     * @param string $type
     * @param string $displayAs
     * @param string $status
     * @param int $httpCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function message($message, $type = MessageType::INFO, $displayAs = DisplayAs::PAGE, $status = 'success', $httpCode = 200): \Illuminate\Http\JsonResponse
    {
        $cm = new ClientMessages();
        $cm->add($message, $type, $displayAs);

        return $this->messages($cm, $status, $httpCode);

    }

    public function error($message, $type = MessageType::DANGER, $displayAs = DisplayAs::PAGE, $status = 'error', $httpCode = 400): \Illuminate\Http\JsonResponse
    {
        $cm = new ClientMessages();
        $cm->add($message, $type, $displayAs);

        return $this->messages($cm, $status, $httpCode);
    }
}
