<?php

namespace Meta\Utility\Base;

class ClientMessages
{
    private $messages = [];

    public function __construct()
    {

    }

    // fluent approach to adding new messages
    public function add(string $message, $type, $displayAs = DisplayAs::PAGE)
    {
        $newMessage = compact('message', 'type', 'displayAs');

        if ($displayAs === DisplayAs::FLASH) {
            $currentMessages = \Session::get('messages');
            $currentMessages[] = $newMessage;
            \Session::flash('messages', $currentMessages);

            return $this;
        }

        $this->messages[] = $newMessage;


        return $this;
    }

    public function clear(): ClientMessages
    {
        $this->messages = [];

        return $this;
    }

    public function messages(): array
    {
        return $this->messages;
    }

    public function asResponse(): \Illuminate\Http\JsonResponse
    {
        return \Response::messages($this->messages());
    }

    public function merge(ClientMessages $newMessages = null)
    {
        if (!is_null($newMessages)) {
            $this->messages = array_merge($this->messages, $newMessages->messages);
        }

        return $this;
    }

    public function growl($message, $messageType = MessageType::INFO): ClientMessages
    {
        $this->add($message, $messageType, DisplayAs::GROWL);

        return $this;
    }

    public function page($message, $messageType = MessageType::INFO): ClientMessages
    {
        $this->add($message, $messageType, DisplayAs::PAGE);

        return $this;
    }

    public function flash($message, $messageType = MessageType::INFO): ClientMessages
    {
        $this->add($message, $messageType, DisplayAs::FLASH);

        return $this;
    }


}
