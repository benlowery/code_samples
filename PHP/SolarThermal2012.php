<?php

namespace Meta\Utility\Calculations\Thermal;

use Illuminate\Support\Str;

Class SolarThermal2012
{

    public $postcodes = [
        // abridged
    ];

    public $table1a = [
        1  => '31',
        2  => '28',
        3  => '31',
        4  => '30',
        5  => '31',
        6  => '30',
        7  => '31',
        8  => '31',
        9  => '30',
        10 => '31',
        11 => '30',
        12 => '31',
    ];

    public $table1c = [
        1  => '1.10',
        2  => '1.06',
        3  => '1.02',
        4  => '0.98',
        5  => '0.94',
        6  => '0.90',
        7  => '0.90',
        8  => '0.94',
        9  => '0.98',
        10 => '1.02',
        11 => '1.06',
        12 => '1.10',
    ];

    public $table1d = [

        1  => '41.2',
        2  => '41.4',
        3  => '40.1',
        4  => '37.6',
        5  => '36.4',
        6  => '33.9',
        7  => '30.4',
        8  => '33.4',
        9  => '33.5',
        10 => '36.3',
        11 => '39.4',
        12 => '39.9',

    ];

    // C Value MCS 024

    public $cTable = [
        0 => ['litres' => '0', 'C' => 0],
        1 => ['litres' => '150', 'C' => 417],
        2 => ['litres' => '180', 'C' => 471],
        3 => ['litres' => '200', 'C' => 505],
        4 => ['litres' => '250', 'C' => 586],
        5 => ['litres' => '300', 'C' => 662],
        6 => ['litres' => '500', 'C' => 930],

    ];

    public $nm = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Variable h1 from MCS2012 amendment MCS 0024
    public $tableH1 = [
        1.29,
        0.64,
        1.0,
        1.09,
    ];

    public $tableU3 = [
        [26, 54, 96, 150, 192, 200, 189, 157, 115, 66, 33, 21],
        [30, 56, 98, 157, 195, 217, 203, 173, 127, 73, 39, 24],
        [32, 59, 104, 170, 208, 231, 216, 182, 133, 77, 41, 25],
        [35, 62, 109, 172, 209, 235, 217, 185, 138, 80, 44, 27],
        [36, 63, 111, 174, 210, 233, 204, 182, 136, 78, 44, 28],
        [32, 59, 105, 167, 201, 226, 206, 175, 130, 74, 40, 25],
        [28, 55, 97, 153, 191, 208, 194, 163, 121, 69, 35, 23],
        [24, 51, 95, 152, 191, 203, 186, 152, 115, 65, 31, 20],
        [23, 51, 95, 157, 200, 203, 194, 156, 113, 62, 30, 19],
        [23, 50, 92, 151, 200, 196, 187, 153, 111, 61, 30, 18],
        [25, 51, 95, 152, 196, 198, 190, 156, 115, 64, 32, 20],
        [26, 54, 96, 150, 192, 200, 189, 157, 115, 66, 33, 21],
        [30, 58, 101, 165, 203, 220, 206, 173, 128, 74, 39, 24],
        [29, 57, 104, 164, 205, 220, 199, 167, 120, 68, 35, 22],
        [19, 46, 88, 148, 196, 193, 185, 150, 101, 55, 25, 15],
        [21, 46, 89, 146, 198, 191, 183, 150, 106, 57, 27, 15],
        [19, 45, 89, 143, 194, 188, 177, 144, 101, 54, 25, 14],
        [17, 43, 85, 145, 189, 185, 170, 139, 98, 51, 22, 12],
        [16, 41, 87, 155, 205, 206, 185, 148, 101, 51, 21, 11],
        [14, 39, 84, 143, 205, 201, 178, 145, 100, 50, 19, 9],
        [12, 34, 79, 135, 196, 190, 168, 144, 90, 46, 16, 7],
        [24, 52, 96, 155, 201, 198, 183, 150, 107, 61, 30, 18],
    ];

    // Keyed to 1 because that is the format in the MCS Manual
    public $solar_declination = [
        1  => -20.7,
        2  => -12.8,
        3  => -1.8,
        4  => 9.8,
        5  => 18.8,
        6  => 23.1,
        7  => 21.2,
        8  => 13.7,
        9  => 2.9,
        10 => -8.7,
        11 => -18.4,
        12 => -23.0,
    ];

    public $tableU4 = [
        53.5, 51.6, 51.1, 50.9, 50.5, 51.5, 52.6, 53.5, 54.6, 55.2, 54.4, 53.5,
        52.1, 52.6, 55.9, 56.2, 57.3, 57.5, 57.7, 59.0, 60.1, 54.6,
    ];

    public $k = [

        1 => [26.3, 0.165, 1.44, -2.95, -0.66],
        2 => [-38.5, -3.68, -2.36, 2.89, -0.106],
        3 => [14.8, 3.0, 1.07, 1.17, 2.93],
        4 => [-16.5, 6.38, -0.514, 5.67, 3.63],
        5 => [27.3, -4.53, 1.89, -3.54, -0.374],
        6 => [-11.9, -0.405, -1.64, -4.28, -7.4],
        7 => [-1.06, -4.38, -0.542, -2.72, -2.71],
        8 => [0.0872, 4.89, -0.757, -0.25, -0.991],
        9 => [-0.191, -1.99, 0.604, 3.07, 4.59],

    ];

    public function regionFromPostcode($postcode)
    {
        $data = $this->postcodes;


        $postcode = \Str::upper($postcode);

        $first = explode(' ', $postcode)[0];

        $town = preg_replace('/\d/', '', $first);

        foreach ($data as $line) {
            if ($line[0] === $first) {
                return $line[1];
            }
        }

        foreach ($data as $line) {
            if ($line[0] === $town) {
                return $line[1];
            }
        }

        return 'UNKNOWN SET MANUALLY';
    }

    public function solarStorageVolumeFactor($h15)
    {
        $factor = 1 + (0.2 * log($h15));

        // max of 1, Page 74 SAP2012
        if ($factor > 1) {
            $factor = 1;
        }

        return $factor;
    }

    // $t is Total Floor Area
    public function occupancy($t)
    {

        if ($t <= 13.9) {
            $tfa = 1;
        } else {

            $tfa = 1 + 1.76 * (1 - exp(-0.000349 * pow($t - 13.9, 2))) + 0.0013 * ($t - 13.9);
        }

        return $tfa;
    }

    // c-value is the heat lost by volume calculation from MCS 0024
    public function calculateCValue($storage_volume)
    {
        $vol = $storage_volume;

        // The standard says to linearly interpolate however there is no value after 500
        // so we take the value for 300 to 500 and intepolate that
        if ($storage_volume > 500) {
            return 930 + (($storage_volume - 500) * 1.34);
        }

        for ($n = 0; count($this->cTable) < $n; $n++) {
            if ($vol === $this->cTable[$n]['litres']) {
                return $this->cTable[$n]['C'];
            }
        }

        for ($n = 0; $n < count($this->cTable) - 1; $n++) {
            $start = $this->cTable[$n];
            $next = $this->cTable[$n + 1];

            if (($vol > $start['litres']) && ($vol < $next['litres'])) {

                $x = $vol;

                $x0 = $start['litres'];
                $y0 = $start['C'];

                $x1 = $next['litres'];
                $y1 = $next['C'];

                $y = $y0 + (($y1 - $y0) * (($x - $x0) / ($x1 - $x0)));

                return $y;
            }
        }

        return \Response::fail('Error in calculationCValue');
    }

    public function dailyHotWaterDemand($n)
    {
        $vd_average = ((25 * $n) + 36);

        return $vd_average;
    }

    // (d) in MCS 024 1.1
    public function hotWaterUsed($vd_average)
    {
        $energy_used = 4.190 * $vd_average * 365 * (37 / 3600);

        return $energy_used;
    }

    // the sap standard says that NE/NW, E/W, SE/SW are equivalent so to simplify things
    // We'll convert bearings over 180 to their less than 180 equiv (i.e 270 = 90, 315 = 45)

    public function mirrorDegrees($a)
    {

        if ($a < 0) {
            throw new \Exception('Value is less than 360');
        }

        if ($a > 360) {
            throw new \Exception('Value is greater than 360');
        }

        if ($a <= 180) {
            $r = $a;
        } else {
            $r = 180 - ($a - 180);
        }

        return $r;
    }

    /**
     * Return an array of K_Values (SAP2012 Table U5) interpolating between columns for none given values.
     *
     * SAP2012 specifies 5 columns corresponding to N - NE/NW - E/W - SE/SW and S
     * When given a value in degrees it converts it such that NW becomes NE i.e. 315 -> 45
     * as this simplifies the math (and removes the need to handle more columns with the same data)
     *
     * Once given it divides the values by 45 (as each column is offset 45 degrees, i.e. 0/45/90/135/180)
     * and takes a floor and ceiling value (in the case where Val % 45 === 0 the floor and ceil values will be identical)
     *
     * It then iterates over each element in the $k array and applies ever a simple lookup (%45 case)
     * or uses a simple linear interpolation to calculate the "missing" value.
     *
     * returning an array indexed 1-9 with the applicable value.
     *
     * @param $degrees
     *
     * @return array
     */
    public function kValues($degrees)
    {
        // flip on the x-axis
        $degrees = $this->mirrorDegrees($degrees);

        //
        $x0 = (int)floor($degrees / 45);

        // get the end offset in
        $x1 = (int)ceil($degrees / 45);

        $results = [];

        // iterate over all 9 values for k1-k9
        for ($n = 1; $n < count($this->k) + 1; $n++) {
            // this means that we are on a boundary and can use an exact value (i.e 45,90,135)
            if ($x0 === $x1) {
                $results[$n] = $this->k[$n][$x0];
            } // not on a boundary so need to interpolate.
            else {
                // x we know, it's y we want.
                $x = $degrees / 45;

                $y0 = $this->k[$n][$x0];
                $y1 = $this->k[$n][$x1];

                $results[$n] = $y0 + (($y1 - $y0) * (($x - $x0) / ($x1 - $x0)));
            }
        }

        return $results;
    }

    /**
     *  Monlthy Solar Radiation per MCS Solar 2012 Calc
     *
     * @param $region //
     * @param $orient - orient
     * @param $p - tilt
     * @param $m - month
     * @return float
     */
    public function monthlySolarRadiation($region, $orient, $p, $m)
    {

        $k = $this->kValues($this->mirrorDegrees($orient));
        $tilt = deg2rad($p);
        $latitude = deg2rad($this->tableU4[$region]);
        $solar_declination = deg2rad($this->solar_declination[$m]);

        $sinp = sin($tilt / 2);
        $sin2p = pow($sinp, 2);
        $sin3p = pow($sinp, 3);

        $a = ($k[1] * $sin3p) + ($k[2] * $sin2p) + ($k[3] * $sinp);
        $b = ($k[4] * $sin3p) + ($k[5] * $sin2p) + ($k[6] * $sinp);
        $c = ($k[7] * $sin3p) + ($k[8] * $sin2p) + ($k[9] * $sinp + 1);

        $cosv = cos($latitude - $solar_declination);
        $cos2v = pow($cosv, 2);

        $rh_inc = ($a * $cos2v) + ($b * $cosv) + $c;

        // note the $m - 1 it's a zero based array and the months start at 1
        $sol_rad_month = $this->tableU3[$region][$m - 1];

        return $sol_rad_month * $rh_inc;
    }

    public function solarRadiationAnnual($region, $orient, $p)
    {

        $raw_total = 0;
        $adjusted_total = 0;

        $energy = [];

        for ($n = 1; $n < 13; $n++) {

            $raw_month = $this->monthlySolarRadiation($region, $orient, $p, $n);
            $energy[$n]['raw'] = $raw_month;

            $adjusted_month = 0.024 * ($raw_month * $this->daysInMonth($n));
            $energy[$n]['adjusted'] = $adjusted_month;

            $raw_total += $raw_month;
            $adjusted_total += $adjusted_month;
        }

        $energy['raw_total'] = $raw_total;
        $energy['adjusted_total'] = $adjusted_total;

        return $energy;
    }

    public function daysInMonth($m)
    {
        return $this->nm[$m - 1];
    }

    public function mcsSolarThermal2012(
        $occupancy,
        $region,
        $orient,
        $p,
        $aperture,
        $zero_loss,
        $first_loss,
        $second_loss,
        $overshading,
        $hot_water_adjust_factor,
        $water_efficiency,
        $total_water_storage,
        $solar_storage_volume,
        $backup_heater_efficiency
    ) {

        $u3result = $this->solarRadiationAnnual($region, $orient, $p);

        $h1 = $aperture;
        $h2 = $zero_loss;
        $h3 = $first_loss;
        $h3a = $second_loss;

        $h3b = 0.892 * ($h3 + (45 * $h3a));

        $h4 = $h3b / $h2;

        $h5 = $u3result['adjusted_total'];

        $h6 = $overshading;

        $h7 = $h1 * $h2 * $h5 * $h6;

        $vd_average = $this->dailyHotWaterDemand($occupancy); // hot water volum daily

        $u45 = $this->hotWaterUsed($vd_average); // apendix u 45 per MCS2012

        $H = $hot_water_adjust_factor * $water_efficiency * 1;

        $c = $this->calculateCValue($total_water_storage);

        $h8 = $h7 / (($H * $u45) + $c);

        if ($h8 <= 0) {
            $h9 = 0;
        } else {
            $h9 = 1 - exp(-1 / $h8);
        }

        if ($h4 < 20) {
            $h10 = 0.97 - (0.0367 * $h4) + (0.0006 * pow($h4, 2));
        } else {
            $h10 = 0.693 - (0.0108 * $h4);
        }

        if ($h10 < 0) {
            $h10 = 0;
        }

        $h11 = $solar_storage_volume;

        $h12 = $total_water_storage;

        if ($h12 <= 0) {
            $h13 = $h11;
        } else {
            $h13 = $h11 + (0.3 * ($h12 - $h11));
        }

        $h14 = $vd_average;

        $h15 = $h13 / $h14;

        $h16 = $this->solarStorageVolumeFactor($h15);

        $h17 = $h7 * $h9 * $h10 * $h16;

        /* Modified SAP Calculation Finishes */

        $daily_hw_demand = $vd_average * $hot_water_adjust_factor;

        $energy_bill_saving = ($h17 / $backup_heater_efficiency) - $h17;


        return compact(
            'u45',
            'vd_average',
            'hot_water_adjust_factor',
            'daily_hw_demand',
            'energy_bill_saving',
            'h2',
            'h3b',
            'h4',
            'h7',
            'h8',
            'h9',
            'h10',
            'h11',
            'h12',
            'h13',
            'h14',
            'h15',
            'h16',
            'h17'
        );

    }
}
